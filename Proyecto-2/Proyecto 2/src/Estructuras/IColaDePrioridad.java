package Estructuras;

public interface IColaDePrioridad <P extends Comparable<P>, V> {

	public int size();
	public boolean isEmpty();
	public void enqueue(V value, P priority);
	public V unqueueMax();
	
}
