package Estructuras;

import java.util.Collection;
import java.util.Iterator;

import mundo.IIdentificable;


public interface ICola<T extends IIdentificable> extends Collection<T>{

    public T getFirst();

    public T getLast();

    public void encolar(T valor);

    public T desencolar();

    public int size();

    public Iterator<T> iterator();

    public boolean isEmpty();

}
