package Estructuras;

import mundo.IIdentificable;

/**
 * Created by Alejandro on 1/02/16.
 */
public class NodoLista<T extends IIdentificable> {

    private NodoLista anterior;
    private NodoLista siguiente;

    private T elem;

    public NodoLista(T nElem) {
        elem=nElem;
        siguiente=null;
        anterior=null;
    }

    public NodoLista<T> getAnterior() {
        return anterior;
    }

    public NodoLista<T> getSiguiente() {
        return siguiente;
    }

    public T getElem() {
        return elem;
    }

    public void setAnterior(NodoLista anterior) {
        this.anterior=anterior;
    }

    public void setElem(T elem) {
        this.elem=elem;
    }

    public void setSiguiente(NodoLista siguiente) {
        this.siguiente=siguiente;
    }
}
