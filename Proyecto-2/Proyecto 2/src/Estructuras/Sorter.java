package Estructuras;


import Estructuras.ListaDoblementeEncadenada;
import mundo.IIdentificable;

public class Sorter <T extends IIdentificable> {
	
	public enum Ordenamiento {

		MERGE ("Merge Sort"),
		INSERCION ("Inserción");
		
		
		private final String nombre;
		
		
		private Ordenamiento(String nNombre)
		{
			nombre = nNombre;
		}
		
		
		public String darNombre()
		{
			return nombre;
		}
		
		
		public String toString()
		{
			return nombre;
		}
		
		
	}


	public void ordenar(Ordenamiento ordenamiento, IListaDEncadenada<T> lista, boolean ascendente) {
		if(ordenamiento.equals(Ordenamiento.INSERCION)) {
			ordenarInsercion(lista, ascendente);
		} else if(ordenamiento.equals(Ordenamiento.MERGE)) {
			ordenarMerge(lista, ascendente);
		}
	}
	
	private void ordenarInsercion(IListaDEncadenada<T> lista,boolean ascendnte) {

		for(int i = 1; i < lista.size(); i++) {
			boolean done = false;
			T insertion = lista.get(i);
			for(int j = i - 1; j >= 0 && !done; j--) {
				T other = lista.get(j);
				if(ascendnte) {
					if(insertion.compareTo(other)< 0) {
						lista.set(j + 1, other);
						lista.set(j, insertion);
					} else done = true;
				} else {
					if(insertion.compareTo(other) > 0) {
						lista.set(j + 1, other);
						lista.set(j, insertion);
					} else done = true;
				}
			}
		}
	}
	
	private IListaDEncadenada<T> ordenarMerge(IListaDEncadenada<T> lista, boolean orden)
    {   	 
   	 if(lista.size()==1) {
   		 return lista;
   	 }
   	 
   	 IListaDEncadenada<T>izquierda = new ListaDoblementeEncadenada<T>();
   	 for(int i = 0; i<lista.size()/2; i++) {
   		 izquierda.set(i,lista.get(i));
   	 }
   	 
   	IListaDEncadenada<T>derecha = new ListaDoblementeEncadenada<T>();
   	 for(int i = 0; i<lista.size()-lista.size()/2; i++) {
   		 derecha.set(i, lista.get(i+lista.size()/2));
   	 }
   	 
   	 izquierda = ordenarMerge(izquierda, orden);
   	 derecha = ordenarMerge(derecha, orden);
   	 
   	 return merge(izquierda, derecha, orden);   	 
    }

    @SuppressWarnings("unchecked")
	private IListaDEncadenada<T> merge(IListaDEncadenada<T> izquierda, IListaDEncadenada<T> derecha, boolean orden)
    {
    	IListaDEncadenada<T> aux = new ListaDoblementeEncadenada<T>();
   	 
      	 int sentido = 1;
		 if(orden==false) { sentido = -1;}
      	 
   	 int leftIndex = 0, rightIndex = 0;
   	 
   	 for(int k = 0; k < izquierda.size()+derecha.size(); k++) {
   		 if(leftIndex>=izquierda.size()) {
   			 aux.set(k, derecha.get(rightIndex++));
   		 } else if(rightIndex>=derecha.size()) {
   			 aux.set(k, izquierda.get(leftIndex++));
   		 } else if(izquierda.get(leftIndex).compareTo(derecha.get(rightIndex))*sentido<0) {
   			 aux.set(k, izquierda.get(leftIndex++));
   		 } else if(izquierda.get(leftIndex).compareTo(derecha.get(rightIndex))*sentido==0) {
   			 aux.set(k, izquierda.get(leftIndex++));
   		 } else {
   			 aux.set(k, derecha.get(rightIndex++));
   		 }
   	 }

   	 return aux;
    }
	
}
