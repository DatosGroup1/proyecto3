package Estructuras;

import java.util.Collection;
import java.util.Iterator;


public interface IListaDEncadenada<T> extends Collection<T> {

    public T getHead();

    public T getTail();

    public void addFirst(T elem);

    public void addLast(T elem);

    public T removeFirst();

    public T removeLast();

    public int size();

    public boolean isEmpty();

    public Iterator iterator();

    public boolean add(T o);

    public void add(int index, Object element);

    public boolean remove(Object o);

    public Object remove(int index);

    public void clear();

    public T get(int index);
    
    public T get(String id);

    public Object set(int index, Object element);

}
