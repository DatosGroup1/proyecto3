package Estructuras;

import Estructuras.ListaDeBusquedaSecuencial.Nodo;
import mundo.IIdentificable;



public class TablaHash<K extends Comparable<K> ,V extends IIdentificable> implements ITablaHash<K,V>{


	
	private float factorCargaMax;

	
	private ListaDeBusquedaSecuencial<K, V> [] tabla;

	
	private int count;

	
	private int capacidad;



	@SuppressWarnings("unchecked")
	public TablaHash(){
		capacidad = (4);
		tabla = (ListaDeBusquedaSecuencial<K, V>[])new ListaDeBusquedaSecuencial[capacidad];
		for(int i=0; i < capacidad;i++) {
			tabla[i] = new ListaDeBusquedaSecuencial<>();
		}
		count=0;
		factorCargaMax=(float)0.5;
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		this.capacidad = capacidad;
		tabla = (ListaDeBusquedaSecuencial<K, V>[])new ListaDeBusquedaSecuencial[capacidad];
		for(int i=0; i < capacidad;i++) {
			tabla[i] = new ListaDeBusquedaSecuencial<>();
		}
		count=0;
		this.factorCargaMax=factorCargaMax;

	}
	
	public int size() {
		return count;
	}

	public void put(K llave, V valor){

		ListaDeBusquedaSecuencial<K, V> list = tabla[hash(llave)];
		boolean ans = list.put(llave, valor);
		if(ans){count++;}
		if(count>capacidad*factorCargaMax){resize(capacidad*2);}
	}

	public V get(K llave){

		ListaDeBusquedaSecuencial<K, V> list = tabla[hash(llave)];
		return list.get(llave);		
	}


	public V delete(K llave){
		ListaDeBusquedaSecuencial<K, V> list = tabla[hash(llave)];
		V ans = list.delete(llave);
		if(ans!=null) { count--;}
		if(count>0&&count<=capacidad*factorCargaMax/4){resize(capacidad/2);}
		return ans;
	}

	//Hash
	private int hash(K llave)
	{
		return (llave.hashCode() & 0x7FFFFFFF)%capacidad;
	}
	
	private void resize(int nuevaCapacidad) {
		int antiguaCapacidad = capacidad;
		capacidad = nuevaCapacidad;
		
		TablaHash<K,V> t = new TablaHash<K,V>(capacidad, factorCargaMax);		
		
		for(int i=0;i<antiguaCapacidad;i++) {
			ListaDeBusquedaSecuencial<K, V> list = tabla[i];
			if (list.size()!=0) {
					Object[] keys = list.getKeys();
					Object[] values = list.getValues();

					for(int k=0; k<values.length;k++) {
						t.put((K)keys[k], (V)values[k]);
					}
				
			}			
		}
		
		setTabla(t.darTabla());
		
		
	}
	
	private ListaDeBusquedaSecuencial<K, V>[] darTabla() {
		return tabla;
	}
	private void setTabla(ListaDeBusquedaSecuencial<K, V>[] t) {
		tabla = t;
	}
	public ListaDoblementeEncadenada<V> getAll()
	{
		ListaDoblementeEncadenada<V> resp = new ListaDoblementeEncadenada<V>();
		for(ListaDeBusquedaSecuencial<K, V> lD : tabla)
		{
			Nodo este = lD.getPrimero();
			while(este!= null)
			{
				resp.add((V) este.valor);
				este=este.siguiente;
			}
		}
		return resp;
	}
}