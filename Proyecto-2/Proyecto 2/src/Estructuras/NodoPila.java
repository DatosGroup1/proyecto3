package Estructuras;

import mundo.IIdentificable;

public class NodoPila<T extends IIdentificable> 
{
	
	T item;
	
	NodoPila siguiente;
	
	public NodoPila(T pT) 
	{
		item = pT;
	}
	
	public NodoPila getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(NodoPila siguiente) {
		this.siguiente = siguiente;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T valor) {
		this.item = valor;
	}
	

}
