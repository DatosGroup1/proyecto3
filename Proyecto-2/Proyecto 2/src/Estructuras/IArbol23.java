package Estructuras;

import java.util.Collection;
import java.util.Iterator;

import mundo.IIdentificable;


public interface IArbol23<K extends Comparable<K>, T > extends Collection<T>{

    public NodoArbol23<K,T> getRaiz();
    
    public void agregar(T valor, K llave);

    public NodoArbol23<K,T> buscar(K llave);
    
    public void eliminar(K llave);

    public boolean isEmpty();

}
