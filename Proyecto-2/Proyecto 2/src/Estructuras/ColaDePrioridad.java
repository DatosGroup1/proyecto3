package Estructuras;

import java.util.Iterator;

public class ColaDePrioridad<P extends Comparable<P>, V> implements Iterable<V>, IColaDePrioridad<P, V>{

	private Object[] heap;
	private int size;
	private boolean ordenadaAlMayor;
	
	public int size() {
		return size;
	}
	
	public boolean isEmpty() {
		return size==0;
	}
	
	public ColaDePrioridad(boolean alMayor) {
		heap = new Object[5];
		heap[0]=null;
		size=0;
		ordenadaAlMayor = alMayor;
	}
	
	public void enqueue(V value, P priority){
		boolean noSpace=true;
		if(size==0) {
			heap[1]=new HeapObject(priority, value);
			size++;
			return;
		}
		for(int i = size+1;i>0&&noSpace;i--) {
			if(heap[i]!= null) {
				heap[i+1] = new HeapObject(priority, value);
				swim (i+1);
				size++;
				noSpace=false;
			}
		}

		checksize();
	}
	
	public V unqueueMax() {
		boolean noSpace=true;
		V ans = ((HeapObject)heap[1]).value;
		if(ans==null) {
			return null;
		} else { heap[1]=null; size--;}
		
		for(int i = size+1;i>0&&noSpace;i--) {
			if(heap[i]!= null) {
				exchange(1, i);
				sink(1);
				noSpace=false;
			}
		}
		checksize();
		return ans;
	}
	
	private void checksize() {
		if(size>heap.length/2) {
			
			Object[] aux = new Object[heap.length*2];
			int i =1;
			while(heap[i]!=null) {
				aux[i]=heap[i];
				i++;
			}
			heap=aux;
		}
		else if(heap.length/4>=5&&size<heap.length/4 ) {
			Object[] aux = new Object[heap.length/2];
			int i =1;
			while(heap[i]!=null) {
				aux[i]=heap[i];
				i++;
			}
			heap=aux;
		}
	}
	
	private void swim(int index) {
		while(index/2>=1) {
			if(less((HeapObject)heap[index/2], (HeapObject)heap[index])){
				exchange(index, index/2);
				index/=2;
			} else {
				return;
			}
		}
	}
	
	private void sink(int index) {
		while(index*2<=size) {
			int j = index*2;
			if(j<size&&less((HeapObject)heap[j],(HeapObject)heap[j+1])) {
				j++;
			}
			if(!less((HeapObject)heap[index],(HeapObject)heap[j])) {
				return;
			}
			exchange(index, j);
			index=j;
		}
	}

	private boolean less(HeapObject a, HeapObject b) {
		return ordenadaAlMayor?a.priority.compareTo(b.priority)<0:a.priority.compareTo(b.priority)>=0;
	}
	
	private void exchange (int indexA, int indexB) {
		HeapObject ho = (HeapObject)heap[indexA];
		heap[indexA] = heap[indexB];
		heap[indexB] = ho;
	}
	
	@Override
	public Iterator<V> iterator() {
		return new IteradorColaPrioridad();
	}
	
	class HeapObject {
		P priority;
		V value;
		public HeapObject(P pPriority, V pValue){
			priority=pPriority;
			value=pValue;
		}
	}
	
	class IteradorColaPrioridad implements Iterator<V>{
		
		HeapObject actual = (HeapObject)heap[0];
		int index = 0;
		
		@Override
		public boolean hasNext() {
			if(index+1>=heap.length) {
				index = 0;
				return false;
			}
			return true;
		}

		@Override
		public V next() {
			if(index+1>=heap.length) {
				index = 0;
				return null;
			}
			if(index==0){
				index++;
				actual = (HeapObject)heap[index++];
			}
			V answer =  actual.value;
			actual = (HeapObject)heap[index++];
			return answer;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}