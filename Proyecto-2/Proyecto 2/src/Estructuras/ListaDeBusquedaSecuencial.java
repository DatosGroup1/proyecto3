package Estructuras;

public class ListaDeBusquedaSecuencial<K,V> {
	private Nodo primero;
	private int size=0;
	
	public class Nodo {
		K llave;
		V valor;
		Nodo siguiente;
		
		public Nodo(K llave, V valor, Nodo siguiente) {
			this.llave=llave;
			this.valor=valor;
			this.siguiente=siguiente;
		}
		
	}
	public V get(K llave) {
		for(Nodo i=primero; i!=null; i=i.siguiente) {
			if(llave.equals(i.llave)) {
				return i.valor;
			}
		}
		return null;
	}
	public boolean put(K llave, V valor) {
		for(Nodo i=primero; i!=null; i=i.siguiente) {
			if(llave.equals(i.llave)) {
				i.valor=valor;
				return false;
			}
		}
		primero=new Nodo(llave, valor, primero);
		size++;
		return true;
	}
	public V delete(K llave) {
		Nodo anterior = primero;
		if (anterior==null) {
			return null;
		}
		Nodo i=primero.siguiente;
		for(; i!=null; i=i.siguiente, anterior=anterior.siguiente) {
			if(llave.equals(i.llave)) {
				anterior.siguiente=i.siguiente;
				i.siguiente=null;
				size--;
				return i.valor;
			}
		}
		return null;
	}
	public int size() {
		return size;
	}
	
	public Object[] getKeys() {
		Object[]ans=new Object[size];
		int j = 0;
		for(Nodo i=primero; i!=null; i=i.siguiente) {
			ans[j]=i.llave;
			j++;
		}
		return ans;
	}
	public Object[] getValues() {
		Object[]ans=new Object[size];
		int j = 0;
		for(Nodo i=primero; i!=null; i=i.siguiente) {
			ans[j]=i.valor;
			j++;
		}
		return ans;
	}
	public Nodo getPrimero()
	{
		return primero;
	}
	
}
