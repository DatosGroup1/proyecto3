package Estructuras;


import java.util.Collection;
import java.util.Iterator;

import mundo.IIdentificable;



public class ListaDoblementeEncadenada<T extends IIdentificable> implements IListaDEncadenada<T> {

    private NodoLista<T> head;
    private NodoLista<T> tail;
    private int size;

    public ListaDoblementeEncadenada() {
        size=0;
        head=null;
        tail=null;
    }

    public T getHead() {
        return head.getElem();
    }

    public T getTail() {
        return tail.getElem();
    }

    @Override
    public void addFirst(T elem) {
        T el = elem;
        if(size!=0) {
            NodoLista<T> insert =  new NodoLista<>(el);
            insert.setSiguiente(head);
            head.setAnterior(insert);
            head=insert;
            size++;
        } else {
            head = new NodoLista<>(el);
            tail=head;
            size++;
        }
    }

    @Override
    public void addLast(T elem) {
        T el =  elem;
        if(size!=0) {
            NodoLista<T> insert = new NodoLista<>(el);
            insert.setAnterior(tail);
            tail.setSiguiente(insert);
            tail=insert;
            size++;
        } else {
            tail= new NodoLista<>(el);
            head=tail;
            size++;
        }
    }


    public T removeFirst() {
    	if(head!=null&&head.equals(tail)) {
    		T e =head.getElem();
    		head = null;
    		tail = head;
    		return e;
    	}
    	
    	else if(head!=null) {
            NodoLista<T> newHead = head.getSiguiente();
            newHead.setAnterior(null);
            T e =head.getElem();
            head=newHead;
            return e;
        }
        return null;
    }

    public T removeLast() {
    	
    	if(tail!=null&&tail.equals(head)) {
    		T ans = tail.getElem();
    		head = null;
    		tail = head;
    		return ans;
    	}
        if (tail != null) {
            NodoLista<T> newTail = tail.getAnterior();
            T ans = tail.getElem();
            newTail.setSiguiente(null);
            tail = newTail;

            return ans;
        }
        return null;
    }

        @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }


    @Override
    public Iterator <T> iterator() {
        return new ListaDEIterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean add(T o) {
        T insert = o;
        addFirst(insert);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        T elem = (T)o;
        if(size!=0) {
            NodoLista<T> actual = head;
            for (int i=0; i<size; i++) {
                if(actual.getElem().getId().equals(elem.getId())) {
                	
                	if(size==1) {
                		removeFirst();
                	} else if (size == 2) {
                		if(i==0) {
                			removeFirst();
                		} else {
                			removeLast();
                		}
                	} else {
                		actual.getAnterior().setSiguiente(actual.getSiguiente());
	                    actual.getSiguiente().setAnterior(actual.getAnterior());
	                    actual.setAnterior(null);
	                    actual.setSiguiente(null);
                	}
                    
                    size--;
                    return true;
                }
                actual=actual.getSiguiente();
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

        if(size!=0) {
            NodoLista<T> actual = head.getSiguiente();
            for(int i=1; i<size; i++) {
                actual.setAnterior(null);
                NodoLista<T> sig = actual.getSiguiente();
                actual.setSiguiente(null);
                actual=sig;
            }
            head=actual;
            tail=head;
            size=0;
        }

    }

    @Override
    public T get(int index) {

        if(size!=0) {
            int idx = 0;
            NodoLista<T> actual = head;
            for (int i=0; i<size; i++) {
                if (idx == index) {
                    return actual.getElem();
                }
                idx++;
                actual = actual.getSiguiente();
            }
        }
        return null;
    }

    @Override
    public Object set(int index, Object element) {
        if(size!=0&&index<size) {
            T elem = (T)element;
            int idx = 0;
            NodoLista<T> actual = head;
            for (int i=0; i<size; i++) {
                if (idx == index) {
                    T ans = actual.getElem();
                    actual.setElem(elem);
                    return ans;
                }
                idx++;
                actual = actual.getSiguiente();
            }
        }
        return null;
    }

    @Override
    public void add(int index, Object element) {
        if(size!=0) {
         T elem = (T)element;
            int idx = 0;
            NodoLista<T> actual = head;
            for (int i=0; i<size; i++) {
                if (idx == index) {
                    actual.setElem(elem);
                    break;
                }
                idx++;
                actual = actual.getSiguiente();
            }
        }
    }

    @Override
    public Object remove(int index) {
        if(size!=0) {
            int idx = 0;
            NodoLista<T> actual = head;
            for (int i=0; i<size; i++) {
                if (idx == index) {
                    T ans = actual.getElem();
                    if(i==0) {
                        removeFirst();
                    } else if (i==size-1) {
                        removeLast();
                    } else {
                        actual.getAnterior().setSiguiente(actual.getSiguiente());
                        actual.getSiguiente().setAnterior(actual.getAnterior());
                        actual.setAnterior(null);
                        actual.setSiguiente(null);
                    }
                    size--;
                    return ans;
                }
                idx++;
                actual = actual.getSiguiente();
            }
        }
        return null;
    }

    private class ListaDEIterator implements Iterator<T> {

        private NodoLista<T> actual = head;

        @Override
        public boolean hasNext() {
            return actual!=null;
        }

        @Override
        public T next() {
            T ans = actual.getElem();
            actual=actual.getSiguiente();
            return ans;
        }

        @Override
        public void remove() {

        }
    }

	@Override
	public T get(String id) {
		if(size!=0) {
            NodoLista<T> actual = head;
            for (int i=0; i<size; i++)  {
            	T compare = actual.getElem();
                if (compare.getId().equals(id)) {
                    return actual.getElem();
                }
                actual = actual.getSiguiente();
            }
        }
        return null;
	}

}