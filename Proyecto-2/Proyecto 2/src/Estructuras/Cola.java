package Estructuras;

import java.util.Collection;
import java.util.Iterator;

import mundo.IIdentificable;

public class Cola<T extends IIdentificable> implements ICola<T>{
	
	private ItemCola<T> primero;
	private ItemCola<T> ultimo;
	private int tamanio;
	

	public T getFirst() {
		return primero.getValor();
	}

	public T getLast() {
		return ultimo.getValor();
	}
	
	public void encolar(T valor){
		ItemCola<T> nuevo = new ItemCola<T>(valor);
		if(primero == null)
		{
			primero = ultimo = nuevo;
		}
		else{
			ultimo.setSiguiente(nuevo);
			ultimo = nuevo;
		}
		tamanio++;
	}
	
	public T desencolar(){
		if (isEmpty())
			return null;
		T ret = primero.getValor();
		primero = primero.getSiguiente();
		tamanio--;
		return ret;
		
	}

	@Override
	public int size() {
		return tamanio;
	}

	@Override
	public boolean isEmpty() {
		return tamanio==0;
	}

	@Override
	public boolean contains(Object o) {
		return false;
	}

	@Override
	public Iterator<T> iterator() {
		return new ColaIterator();
		
		
	}

	@Override
	public Object[] toArray() {
		return new Object[0];
	}

	@Override
	public <T1> T1[] toArray(T1[] a) {
		return null;
	}

	@Override
	public boolean add(T t) {
		encolar(t);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		desencolar();
		return true;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return false;
	}

	@Override
	public void clear() {
		while(primero!=null) {
			desencolar();
		}
	}

	private class ColaIterator implements Iterator<T> {
		
		private ItemCola<T> actual = primero;

		@Override
		public boolean hasNext() {
			return actual!=null;
		}

		@Override
		public T next() {
			T ret = actual.getValor();
			actual = actual.getSiguiente();
			return ret;
		}

		@Override
		public void remove() {
			
		}
		
	}

}
