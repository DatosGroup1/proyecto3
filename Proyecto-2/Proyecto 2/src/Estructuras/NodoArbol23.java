package Estructuras;

import mundo.IIdentificable;

public class NodoArbol23<K extends Comparable<K>,T > 
{
	public final static boolean ROJO = false;
	public final static boolean NEGRO = true;
	K llave;				
	T item;
	NodoArbol23<K,T> izquierdo;
	NodoArbol23<K,T> derecho;
	boolean color;
	
	
	public NodoArbol23(K pLlave, T pT) 
	{
		item = pT;
		llave = pLlave;
		izquierdo = null;
		derecho= null;
		color = ROJO;
	}
	
	public NodoArbol23<K,T> getIzquierdo() {
		return izquierdo;
	}
	
	public NodoArbol23<K,T> getDerecho() {
		return derecho;
	}
	
	public NodoArbol23<K,T> eliminar(K pllave)
	{
		NodoArbol23<K,T>resultado = null;
		if ( pllave.compareTo(llave) == 0 )
		{
			if ( izquierdo == null && derecho == null )
				resultado = null;
			else if ( izquierdo == null )
				resultado = derecho;
			else if ( derecho == null )
				resultado = izquierdo;
			else
			{ 
			 NodoArbol23<K,T> reemplazo = derecho.darMenor( );
			 derecho = derecho.eliminar( reemplazo.getLlave() );
			 reemplazo.izquierdo = this.izquierdo;
			 reemplazo.derecho = this.derecho;
			 resultado = reemplazo;
			}
		}
		else if ( pllave.compareTo(llave) < 0 )
		{ 
			izquierdo = izquierdo.eliminar( pllave );
			resultado = this;
		}
		else
		{ 
			izquierdo = izquierdo.eliminar( pllave );
			resultado = this;
		}
		return resultado;
	}

	public NodoArbol23<K,T> agregar(NodoArbol23<K,T> agr,Arbol23 total) 
	{
		NodoArbol23<K,T> analiza = this;
		int otra = llave.compareTo(agr.getLlave());
		if(otra == 0)
			return this;
		else if(otra<0)
		{
			if (derecho == null)
			{
				derecho = agr;
				total.setSize(total.size()+1);
			}
			else
				derecho = derecho.agregar(agr,total);
		}
		else if(otra>0)
		{
			if (izquierdo == null)
			{
				izquierdo = agr;
				total.setSize(total.size()+1);
			}
			else
				izquierdo = izquierdo.agregar(agr,total);
		}

		if(analiza.getDerecho() != null&& analiza.getDerecho().getColor()== ROJO&&analiza.getIzquierdo() != null&&analiza.getIzquierdo().getColor()== NEGRO)
		{
			analiza.getDerecho().setColor(analiza.getColor());
			analiza.setColor(ROJO);
			NodoArbol23<K,T> temp = analiza.getDerecho().getIzquierdo();
			analiza.getDerecho().setIzquierdo(analiza);
			analiza= analiza.getDerecho();
			analiza.getIzquierdo().setDerecho(temp);								
		}
		if(analiza.getIzquierdo() != null&&analiza.getIzquierdo().getColor()== ROJO &&analiza.getIzquierdo().getIzquierdo()!=null&& analiza.getIzquierdo().getIzquierdo().getColor()== ROJO)
		{
			analiza.getIzquierdo().setColor(analiza.getColor());
			analiza.setColor(ROJO);
			NodoArbol23<K,T> temp = analiza.getIzquierdo().getDerecho();
			analiza.getIzquierdo().setDerecho(analiza);
			analiza= analiza.getIzquierdo();
			analiza.getDerecho().setIzquierdo(temp);				
		}
		if(analiza.getIzquierdo() != null&&analiza.getIzquierdo().getColor()== ROJO && analiza.getDerecho() != null&&analiza.getDerecho().getColor()== ROJO)
		{
			analiza.getIzquierdo().setColor(NEGRO);
			analiza.getDerecho().setColor(NEGRO);
			analiza.setColor(ROJO);
		}
		return analiza;
	}
	
	private void setIzquierdo(NodoArbol23<K,T> p) 
	{
		izquierdo = p;
	}
	private void setDerecho(NodoArbol23<K,T> p) 
	{
		derecho = p;
	}

	public boolean getColor() 
	{
		return color;
	}

	public NodoArbol23<K,T> buscar(K otra) 
	{
		NodoArbol23<K,T> buscado = null;
		if(llave.equals(otra))
			buscado = this;
		else if(llave.compareTo(otra)<0)
		{
			if(derecho == null)
				buscado = null;
			else 
				buscado = derecho.buscar(otra);
		}
		else if(llave.compareTo(otra)>0)
		{
			if(izquierdo == null)
				buscado = null;
			else 
				buscado = izquierdo.buscar(otra);
		}
		return buscado;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T valor) {
		this.item = valor;
	}
	
	public K getLlave() {
		return llave;
	}

	public void setLlave(K valor) {
		this.llave = valor;
	}

	public void setColor(boolean t) 
	{
		color = t;
	}
	public NodoArbol23<K,T> darMenor()
	{
		NodoArbol23<K,T> resultado = null;
		if(izquierdo == null)
			resultado = this;
		else 
			resultado = izquierdo.darMenor();
		return resultado;
	}
	public NodoArbol23<K,T> darMayor()
	{
		NodoArbol23<K,T> resultado = null;
		if(derecho == null)
			resultado = this;
		else 
			resultado = derecho.darMenor();
		return resultado;
	}
}
