package Estructuras;

import mundo.IIdentificable;

public interface ITablaHash <K extends Comparable<K> ,V extends IIdentificable> {

	public int size();
	public void put(K llave, V valor);
	public V get(K llave);
	public V delete(K llave);
	public ListaDoblementeEncadenada<V> getAll();
	
}
