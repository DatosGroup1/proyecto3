package Estructuras;

import java.util.Collection;

import mundo.IIdentificable;

public interface IPila<T extends IIdentificable> extends Collection<T>
{
	public void push(T t);
	
	public T pop();
	
	public int size();
	

	public boolean isEmpty();

	

}
