package Estructuras;

import java.util.Collection;
import java.util.Iterator;

import mundo.IIdentificable;

public class Arbol23<K extends Comparable<K>,T > implements IArbol23<K,T>{
	
	private NodoArbol23<K,T> raiz;
	private int size;

	
	
	public Arbol23()
	{
		size = 0;
		raiz = null;
	}
	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() 
	{
		raiz= null;
		
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		
		return size;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodoArbol23<K,T> getRaiz() 
	{
		return raiz;
	}

	public void agregar(T valor, K llave) 
	{
		NodoArbol23<K,T> agr = new NodoArbol23<K,T>(llave, valor);
		if(raiz==null)
		{
			raiz=agr;
			size++;
		}
		else
			raiz = raiz.agregar(agr,this);
		raiz.setColor(NodoArbol23.NEGRO);
	}

	public NodoArbol23<K,T> buscar(K llave) 
	{
		return raiz.buscar(llave);
	}

	public void eliminar(K llave) 
	{
		raiz.eliminar(llave);
	}

	@Override
	public boolean isEmpty() {
		if (raiz == null)
			return true;
		return false;
	}

	@Override
	public boolean add(T arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	public void setSize(int i) 
	{
		size = i;	
	}
	

	

	
}
