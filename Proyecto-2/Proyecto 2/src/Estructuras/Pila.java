package Estructuras;

import java.util.Collection;
import java.util.Iterator;

import mundo.IIdentificable;



public class Pila<T extends IIdentificable> implements IPila<T>
{
	private NodoPila<T> primero ;
	private int size;
	@Override
	public boolean add(T e) 
	{
		T res = (T)e;		
		push(res);
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) 
	{
		
		return false;
	}

	@Override
	public void clear() 
	{
		
		primero = null;
		size = 0;
	}
	

	@Override
	public boolean containsAll(Collection<?> c) 
	{
				return false;
	}

	@Override
	public boolean remove(Object o) 
	{
		pop();
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) 
	{
		
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) 
	{
		
		return false;
	}

	@Override
	public Object[] toArray() 
	{
		
		return null;
	}



	

	@Override
	public T pop() 
	{
		T item = primero.getItem();
		primero = primero.getSiguiente();
		size--;
		return item;
	}

	@Override
	public int size() 
	{
		
		return size;
	}

	

	@Override
	public boolean isEmpty() {
		
		return primero == null;
	}

	@Override
	public void push(T t) 
	{
		NodoPila<T> vPrim = primero;
		primero = new NodoPila<T>(t );
		primero.setSiguiente(vPrim);
		size++;		
	}



	@Override
	public Iterator<T> iterator() {
		
		return null;
	}

	@Override
	public boolean contains(Object arg0) {
		return false;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		
		return null;
	}

	

	
	
  }
	
	


