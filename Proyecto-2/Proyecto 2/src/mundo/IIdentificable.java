package mundo;

public interface IIdentificable extends Comparable<Object> {
		public String getId();
}