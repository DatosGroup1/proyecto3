package mundo;


/**
 * Created by Alejandro on 11/02/16.
 */
public class Zone implements IIdentificable{

    private int zona;

    private String paseador_nombre;
    private String paseador_apellido;
    private double ubicacion_max_noroccidente_latitud;
    private double ubicacion_max_noroccidente_longitud;
    private double ubicacion_max_suroriente_latitud;
    private double ubicacion_max_suroriente_longitud;

    public Zone(int zona, String paseador_nombre, String paseador_apellido, double ubicacion_max_noroccidente_latitud, double ubicacion_max_noroccidente_longitud, double ubicacion_max_suroriente_latitud, double ubicacion_max_suroriente_longitud) {
        this.zona = zona;
        this.paseador_nombre = paseador_nombre;
        this.paseador_apellido = paseador_apellido;
        this.ubicacion_max_noroccidente_latitud = ubicacion_max_noroccidente_latitud;
        this.ubicacion_max_noroccidente_longitud = ubicacion_max_noroccidente_longitud;
        this.ubicacion_max_suroriente_latitud = ubicacion_max_suroriente_latitud;
        this.ubicacion_max_suroriente_longitud = ubicacion_max_suroriente_longitud;
    }

    public int getZona() {
        return zona;
    }

    public String getPaseador_nombre() {
        return paseador_nombre;
    }

    public String getPaseador_apellido() {
        return paseador_apellido;
    }

    public double getUbicacion_max_noroccidente_latitud() {
        return ubicacion_max_noroccidente_latitud;
    }

    public double getUbicacion_max_noroccidente_longitud() {
        return ubicacion_max_noroccidente_longitud;
    }

    public double getUbicacion_max_suroriente_latitud() {
        return ubicacion_max_suroriente_latitud;
    }

    public double getUbicacion_max_suroriente_longitud() {
        return ubicacion_max_suroriente_longitud;
    }

	@Override
	public String getId() {
		return zona+"";
	}
	public boolean estaAdentro(double lat, double lon)
	{		
		if( lat<ubicacion_max_noroccidente_latitud && lat>ubicacion_max_suroriente_latitud && lon<ubicacion_max_suroriente_longitud && lon>ubicacion_max_noroccidente_longitud )
			return true;
		return false;
	}

	@Override
	public int compareTo(Object o) {
		Zone dos = (Zone)o;
		return (new Integer(zona)).compareTo(new Integer(dos.getZona()));
	}
}
