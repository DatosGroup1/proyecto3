package mundo;

import java.util.Date;

import Estructuras.ListaDoblementeEncadenada;

/**
 * Created by Alejandro on 11/02/16.
 */
public class Dog implements IIdentificable{

    private int codigo_collar;
    private String nombre;
    private  String raza;
    private int edad_meses;
    private int pul_max_pormin;
    private int pul_normal_pormin_inferior;
    private int pul_normal_pormin_superior;
    private int pul_min_pormin_inferior;
    private int pul_min_pormin_superior;
    private int zona;
    private ListaDoblementeEncadenada<Event> eventos;
    private double promedio_freq_cardiaca;
    private double promedio_Actividades;
    private int cantEmergencias;

    

	public Dog(int codigo_collar, String nombre, String  raza, int edad_meses, int pul_max_pormin, int pul_normal_pormin_inferior,int pul_normal_pormin_superior, int pul_min_pormin_superior, int pul_min_pormin_inferior, int zona) {
        this.codigo_collar=codigo_collar;
        this.nombre = nombre;
        this.raza = raza;
        this.edad_meses = edad_meses;
        this.pul_max_pormin = pul_max_pormin;
        this.pul_normal_pormin_inferior = pul_normal_pormin_inferior;
        this.pul_normal_pormin_superior = pul_normal_pormin_superior;
        this.pul_min_pormin_inferior = pul_min_pormin_inferior;
        this.pul_min_pormin_superior = pul_min_pormin_superior;
        this.zona = zona;
        this.eventos=new ListaDoblementeEncadenada<>();
        this.promedio_freq_cardiaca = 0.0;      
    }

    public int getCodigo_collar() {
        return codigo_collar;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRaza() {
        return raza;
    }

    public int getEdad_meses() {
        return edad_meses;
    }

    public int getPul_max_pormin() {
        return pul_max_pormin;
    }

    public int getPul_normal_pormin_inferior() {
        return pul_normal_pormin_inferior;
    }

    public int getPul_normal_pormin_superior() {
        return pul_normal_pormin_superior;
    }

    public int getPul_min_pormin_inferior() {
        return pul_min_pormin_inferior;
    }

    public int getPul_min_pormin_superior() {
        return pul_min_pormin_superior;
    }

    public int getZona() {
        return zona;
    }

    public ListaDoblementeEncadenada<Event> getEventos()
    {
    	return eventos;
    }
    
    @Override
	public String getId() 
	{
		return codigo_collar+"";
	}
    

    public double getPromedio_freq_cardiaca() 
    {
    	return promedio_freq_cardiaca;
    }
    
    public void addEvento(Event e) 
    {
        eventos.add(e);
        calcularPromedioCardio(eventos.size(), e.getRitmo_cardiaco());
    }


    public ListaDoblementeEncadenada<Event> getEventosFecha(Date dia) 
    {
    	ListaDoblementeEncadenada<Event> answer = new ListaDoblementeEncadenada<>();

    	for (Event e : eventos) {
    		if(e.getFecha_hora().getDay()==dia.getDay()&&e.getFecha_hora().getMonth()==dia.getMonth()) {
    			answer.add(e);
    		}
    	}
		return answer;
    }
    
    public ListaDoblementeEncadenada<Event> getEventosEntreFecha(Date inicial, Date finale, boolean tipo, boolean alarma) 
    {
    	ListaDoblementeEncadenada<Event> answer = new ListaDoblementeEncadenada<>();

    	for (Event e : eventos) 
    	{
    		if(e.getFecha_hora().before(finale) && e.getFecha_hora().after(inicial)) 
    		{
    			answer.add(e);
    		}
    	}
		return answer;
    }
    private void calcularPromedioCardio(int cantidadEventos, int freqCar) {
        promedio_freq_cardiaca *= (double)(cantidadEventos-1)/(double)cantidadEventos;
        promedio_freq_cardiaca += ((double)freqCar/(double)cantidadEventos);
    }

    public String calcularPromedioGeo() {
        int cantDurmiendo = 0;
        int cantComiendo = 0;
        int cantCaminando = 0;
        int cantCorriendo = 0;
        for(Event e  : eventos) {
        	if(e.getRitmo_cardiaco()>= pul_min_pormin_inferior && e.getRitmo_cardiaco()< pul_min_pormin_superior)
        	{
        		cantDurmiendo++;
        	}
        	else if(e.getRitmo_cardiaco()>= pul_min_pormin_superior && e.getRitmo_cardiaco()< pul_normal_pormin_inferior)
        	{
        		cantComiendo++;
        	}
        	else if(e.getRitmo_cardiaco()>= pul_normal_pormin_inferior && e.getRitmo_cardiaco()< pul_normal_pormin_superior)
        	{
        		cantCaminando++;
        	}
        	else if(e.getRitmo_cardiaco()>= pul_normal_pormin_superior && e.getRitmo_cardiaco()<= pul_normal_pormin_superior)
        	{
        		cantCorriendo++;
        	}
        }
        return cantDurmiendo+"/"+cantComiendo+"/"+cantCaminando+"/"+cantCorriendo;
    }

	public boolean sinEjercicio(int horas) 
	{		
		Date fi = eventos.getTail().getFecha_hora();
		Date in = new Date(fi.getTime()-(3600000*horas));		
		ListaDoblementeEncadenada<Event> datos = getEventosEntreFecha(in, fi, true, false);		
		for(Event e: datos)
		{
			if(e.getRitmo_cardiaco()>=pul_normal_pormin_superior)
				return false;
		}
		return true;
	}

	@Override
	public int compareTo(Object o) {
		Dog p = (Dog)o;
		return getId().compareTo(p.getId());
	}
	
	
	public void inicializarAtributosNoJASON() {
        this.eventos=new ListaDoblementeEncadenada<Event>();
        this.cantEmergencias = 0;
	}
	

	public int getHorasFueraCasa(Date inicial, Date finale) 
	{
		
		return 0;
	}
	public int getCantEmergencias() 
	{
		return cantEmergencias;
	}
	public void setCantEmergencias(int i) 
	{
		 cantEmergencias = i;
	}

}
