package mundo;


/**
 * Created by Alejandro on 11/02/16.
 */
public class Owner implements IIdentificable{

    private String apellidos;
    private String nombre;
    private int cedula;
    private double direccion_latitud;
    private double direccion_longitud;
    private int[] codigo_collar;
    private String email;
    private long celular;
    private int prioridad;
    

    public Owner(String apellidos, String nombres, int cedula, double direccion_latitud, double direccion_longitud, int[] codigo_collar, String email, long celular, int prioridad) {
        this.apellidos = apellidos;
        this.nombre = nombres;
        this.cedula = cedula;
        this.direccion_latitud = direccion_latitud;
        this.direccion_longitud = direccion_longitud;
        this.codigo_collar = codigo_collar;
        this.email = email;
        this.celular = celular;
        this.prioridad=prioridad;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getNombres() {
        return nombre;
    }

    public int getCedula() {
        return cedula;
    }

    public double getDireccion_latitud() {
        return direccion_latitud;
    }

    public double getDireccion_longitud() {
        return direccion_longitud;
    }

    public int[] getCodigo_collar() {
        return codigo_collar;
    }

    public String getEmail() {
        return email;
    }

    public long getCelular() {
        return celular;
    }

	@Override
	public String getId() {
		return cedula+"";
	}

	@Override
	public int compareTo(Object o) {
		return nombre.compareTo(o.toString());
	}

	public int getPrioridad() {
		return prioridad;
	}

}
