package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import Estructuras.Arbol23;
import Estructuras.Cola;
import Estructuras.ColaDePrioridad;
import Estructuras.ListaDoblementeEncadenada;
import Estructuras.Pila;
import Estructuras.Sorter;
import Estructuras.TablaHash;
import Estructuras.Sorter.Ordenamiento;


public class Admin {
	
	public final static boolean CARDIO = true;	
	public final static boolean GEO= false;
	public final static boolean MAYOR = true;	
	public final static boolean MENOR= false;
	public final static String SALIO = "salio";	
	public final static String LLEGO = "llego";
	public static final double LONGITUD_CENTRO = -74.046202;
	public static final double LATITUD_CENTRO = 4.720659;

    private TablaHash<Integer,Dog> tablaPerros;
    private TablaHash<Integer,Zone> tablaZonas;
    private Pila<Dog> pilaRecogida;
    private ListaDoblementeEncadenada<Dog> noEntregados;
    private TablaHash<Integer, Owner> tablaDuenios;
    private Arbol23<String, ListaDoblementeEncadenada<Event>> historial;
    private ListaDoblementeEncadenada<Event> listaEventos;
    
    private Gson gson;

    public Admin(PrintWriter pr) {
    	gson = new Gson();
    	pilaRecogida = new Pila<>();
    	listaEventos = null;
 
    	historial = new Arbol23<String, ListaDoblementeEncadenada<Event>>();
    	
    	tablaDuenios=new TablaHash<>();
    	tablaPerros = new TablaHash<>();
    	tablaZonas = new TablaHash<>();
    	
    }

    public void inicializarDuenio(PrintWriter pr, String jDuenio) throws FileNotFoundException {
    	BufferedReader br = new BufferedReader(new FileReader(new File(jDuenio)));
        Type tipoLtaDuenios = new TypeToken<ListaDoblementeEncadenada<Owner>>(){}.getType();
        ListaDoblementeEncadenada<Owner> listaDuenios;
        listaDuenios = gson.fromJson(br, tipoLtaDuenios);
        for (Owner d : listaDuenios) {
        	int[]collares = d.getCodigo_collar();
        	for(int i = 0; i<collares.length;i++) {
            	tablaDuenios.put(collares[i], d);
        	}
        }
        try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public void inicializarPerro(PrintWriter pr, String jPerro) throws FileNotFoundException {

        BufferedReader infoPerros = new BufferedReader(new FileReader(new File(jPerro)));
        Type tipoLtaPerros = new TypeToken<ListaDoblementeEncadenada<Dog>>(){}.getType();
        ListaDoblementeEncadenada<Dog> listaPerros = gson.fromJson(infoPerros, tipoLtaPerros);
        for(Dog p: listaPerros) {
        	p.inicializarAtributosNoJASON();
        }
        for (Dog p : listaPerros) {
        	int collar = p.getCodigo_collar();
            tablaPerros.put(collar, p);
        }
        try {
			infoPerros.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public void inicializarArea(PrintWriter pr, String jArea) throws FileNotFoundException {

        BufferedReader dataZona = new BufferedReader(new FileReader(new File(jArea)));
        Type tipoLtaZonas = new TypeToken<ListaDoblementeEncadenada<Zone>>(){}.getType();
        ListaDoblementeEncadenada<Zone> listaZonas =  gson.fromJson(dataZona,tipoLtaZonas);
        
        for (Zone z : listaZonas) {
        	int collar = z.getZona();
            tablaZonas.put(collar, z);
        }
        try {
			dataZona.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
    }
    public void inicializarEventos(PrintWriter pr, String jEventos) throws FileNotFoundException {
    	if(listaEventos != null)
        	guardarHistorial(listaEventos);
    	listaEventos = new ListaDoblementeEncadenada<Event>();
        BufferedReader infoEventos = new BufferedReader(new FileReader(new File(jEventos)));
        Type tipoClaEvento = new TypeToken<ListaDoblementeEncadenada<Event>>(){}.getType();
        ListaDoblementeEncadenada<Event> events = gson.fromJson(infoEventos, tipoClaEvento);
        try {
			infoEventos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        procesarEventos(pr,events);
    }
	@SuppressWarnings("deprecation")
	private void guardarHistorial(ListaDoblementeEncadenada<Event> list) 
    {
    	class IntegerId implements IIdentificable 
    	{
    		Integer num = 0;
    		public IntegerId (int i)
    		{
    			num = i;
    		}
			@Override
			public int compareTo(Object arg0) 
			{				
				return num.compareTo((Integer)arg0);
			}

			@Override
			public String getId() 
			{
				return num+"";
			}
    		
    	}
    	
    	ListaDoblementeEncadenada<IntegerId> usad = new ListaDoblementeEncadenada<IntegerId>();
    	
		for(int i= 0;i<list.size();i++)
		{
			ListaDoblementeEncadenada<Event> temp = new ListaDoblementeEncadenada<Event>();
			Event este = list.get(i);
			if(usad.get(este.getCodigo_collar()+"")== null)
			{
				usad.add(new IntegerId(este.getCodigo_collar()));
				for (int j = i;j<list.size();j++)
				{
					Event e = list.get(j);
					if(e.getCodigo_collar() == este.getCodigo_collar())
						temp.add(e);
				}
				historial.agregar(temp, este.getFecha_hora().getYear()+"-"+este.getFecha_hora().getMonth()+"-"+este.getFecha_hora().getDate()+"-"+este.getCodigo_collar());
			}
		}
	}

	public void procesarEventos(PrintWriter pr,  ListaDoblementeEncadenada<Event> eve) {
    	boolean alarma = false;

    	for(Event e : eve) 
    	{
    		Dog este = tablaPerros.get(e.getCodigo_collar());    		
    		if(!tablaZonas.get(este.getZona()).estaAdentro(e.getUbicacion_actual_latitud(), e.getUbicacion_actual_longitud()))
			{
    			e.setAlarmaGeo(true);
			}
    		if(e.getRitmo_cardiaco()<este.getPul_min_pormin_inferior()|| e.getRitmo_cardiaco()>este.getPul_max_pormin())
			{
    			e.setAlarmaCardio(true);
			}
    		este.getEventos().add(e);
    		if(e.isAlarmaCardio()|| e.isAlarmaGeo()) {
    			  este.setCantEmergencias(este.getCantEmergencias()+1);
    			  alarma = true;
    		} 

    		
    		listaEventos.add(e);
    		
    	}
    	
    }

	private void lanzarAlarma(Dog p, Event e, PrintWriter pr ) 
	{
		pr.println("��ALARMA!!");	
		SimpleDateFormat form = new SimpleDateFormat("yyyy/MMMMM/dd hh:mm aaa");
		if(e.isAlarmaCardio()&& e.isAlarmaGeo())
			pr.println("Problema Cardiaco y fuera de ubicacion"+"\nNombre: " + p.getNombre() +" Fecha: "+ form.format(e.getFecha_hora())+ "\n Cardio: " + e.getRitmo_cardiaco() + " \n Ubicacion(lon/lat): " + e.getUbicacion_actual_longitud()+"/"+e.getUbicacion_actual_latitud());
		else if(e.isAlarmaCardio())
			pr.println("Problema Cardiaco "+"\nNombre: " + p.getNombre() +"\n Fecha: "+ form.format(e.getFecha_hora())+ "\n Cardio: " + e.getRitmo_cardiaco());
		else if(e.isAlarmaGeo())
			pr.println("Fuera de ubicacion"+"\nNombre: " + p.getNombre() +"\nFecha: "+ form.format(e.getFecha_hora())+ " \n Ubicacion(lon/lat): " + e.getUbicacion_actual_longitud()+"/"+e.getUbicacion_actual_latitud());
	}
	
	public int getTamanioEventos()
	{
		return listaEventos.size();
	}
	
	public Dog perroMasAlarmado(boolean tipo, Date inicial, Date finale){
	Dog perr = null;
		if(tipo== CARDIO)
		{
			
			Dog ganador = null;
			int mayor = ganador.getEventosEntreFecha(inicial,finale,CARDIO,true).size();
			for(Dog p : tablaPerros.getAll())
			{
				int valor = p.getEventosEntreFecha(inicial,finale,CARDIO,true).size();
				if(valor>mayor)
				{
					mayor = valor;
					ganador = p;
				}
			}
			perr = ganador;
		}
		if(tipo== GEO)
		{
			int mayor = 0;
			Dog ganador = null;
			for(Dog p : tablaPerros.getAll())
			{
				int valor = p.getEventosEntreFecha(inicial,finale,GEO,true).size();
				if(valor>mayor)
				{
					mayor = valor;
					ganador = p;
				}
			}
			perr = ganador;
		}
		return perr;
	}
	

	public void proveerInformacionSemanal(PrintWriter pr) {
		int week = 1;
		SimpleDateFormat form = new SimpleDateFormat("yyyy/MMMMM/dd hh:mm aaa");
		
		StringBuilder sb = new StringBuilder("");
        sb.append("|                                                                                     |\n");
        sb.append("| EVENTOS DE LA SEMANA:                                                               |\n");
        sb.append("|                                                                                     |\n");
        for(Event e : listaEventos) {
        	sb.append("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
        	sb.append("|  #COLLAR ASOCIADO: "+e.getCodigo_collar()+ "                                                           |\n");
        	sb.append("|                                                                                     |\n");
        	sb.append("|  FECHA: "+form.format(e.getFecha_hora())+ "                                                      |\n");
        	sb.append("|                                                                                     |\n");
        	DecimalFormat df = new DecimalFormat("#.000000");
        	sb.append("|  LATITUD: "+df.format(e.getUbicacion_actual_latitud())+ "                                                                  |\n");
        	sb.append("|  LONGITUD: "+df.format(e.getUbicacion_actual_longitud())+ "                                                               |\n");
        	sb.append("|                                                                                     |\n");
        	String desp = Integer.toString(e.getRitmo_cardiaco()).length()==3?"                                                              |\n":"                                                               |\n";
        	sb.append("|  PULSO REGISTRADO: "+e.getRitmo_cardiaco()+ desp);
        }
        sb.append("---------------------------------------------------------------------------------------\n");
        pr.println(sb.toString() + "Regresando al menu principal...");
	}

	public double promedioDatos(boolean tipo, PrintWriter pr, int collar)
	{
		Dog p = tablaPerros.get(collar);
		double pro= 0;
		if(tipo== CARDIO)
		{			
			pro = p.getPromedio_freq_cardiaca();
		}
		if(tipo== GEO)
		{			
			String[] ans =p.calcularPromedioGeo().split("/");
			pr.println("El promedio de actividad de "+ p.getNombre() + " es de: "+ "\n-Promedio de veces durmiendo: "+ans[0]+ "\n-Promedio de veces comiendo: "+ans[0]+"."+ "\n-Promedio de veces durmiendo: "+ans[1]+"."+ "\n-Promedio de veces caminando: "+ans[2]+"."+ "\n-Promedio de veces corriendo: "+ans[3] +".");
		}
		return pro;
	}
	
	public void historialDiario(PrintWriter pr, Date inicial, Date finale, int collar)
	{
		SimpleDateFormat form = new SimpleDateFormat("yyyy/MMMMM/dd hh:mm aaa");
		Dog p = tablaPerros.get(collar);				
		pr.println("El historial del perro "+ p.getNombre() + " el "+ inicial.getMonth()+"/"+inicial.getDay() +" fue:");
		ListaDoblementeEncadenada<Event> este = p.getEventosEntreFecha(inicial, finale, true, false);
		int i= 1;
		for(Event e : este)
		{
			pr.println("EVENTO " + i);
			pr.println("Ritmo Cardiaco: " + e.getRitmo_cardiaco());
			pr.println("Longitud: " + e.getUbicacion_actual_longitud());
			pr.println("Latitud: " + e.getUbicacion_actual_latitud());
			pr.println("Fecha: " + form.format(e.getFecha_hora()));
			i++;
		}
	}
	
	public Event valorReciente(PrintWriter pr, int collar)
	{		
		Dog p = tablaPerros.get(collar);
		Event e = p.getEventos().getTail();	
		return e;
	}

	private void procesarSalidas(String accion, Dog este) 
	{
		if(accion.equals(SALIO))
			pilaRecogida.add(este);
		else if(accion.equals(LLEGO))
		{			
			Dog sale = pilaRecogida.pop();
			if(sale.getCodigo_collar()!= este.getCodigo_collar())
			{
				noEntregados.add(este);
				reordenarPila(este.getCodigo_collar());
				pilaRecogida.push(sale);
			}
		}	
	}
	
	private void reordenarPila(int collar) 
	{
		ListaDoblementeEncadenada<Dog> saliendo = new ListaDoblementeEncadenada<Dog>();
		boolean terminado = false;
		while(!terminado)
		{
			Dog sale = pilaRecogida.pop();
			if(sale.getCodigo_collar()== collar)
			{
				terminado= true;
			}
			else 
			{
				saliendo.add(sale);
			}
		}
		for(int i =saliendo.size()-1; i>=0 ; i--)
		{
			pilaRecogida.push(saliendo.get(i));
		}
		
	}

	public void perrosNoEntregados(PrintWriter pr)
	{
		pr.println("Los perros no entregados en orden son:");
		int i= 1;
		for(Dog p : noEntregados)
		{
			pr.println("PERRO " + i);
			pr.println("Nombre: " + p.getNombre());
			pr.println("Codigo: " + p.getCodigo_collar());
			pr.println("---------------------------------------------------------------------------------------\n");
			i++;
		}
	}
	
	public void sinEjercicioHoras(PrintWriter pr, int horas)
	{
		ListaDoblementeEncadenada<Dog> sinEjercicio = new ListaDoblementeEncadenada<Dog>();
		for(Dog p :tablaPerros.getAll())
		{
			if(p.sinEjercicio (horas))
				sinEjercicio.add(p);
		}
		pr.println("Los perros sin hacer ejercicio en "+ horas + " horas son:");
		int i= 1;
		for(Dog p : sinEjercicio)
		{
			pr.println("PERRO " + i);
			pr.println("Nombre: " + p.getNombre());
			pr.println("Codigo: " + p.getCodigo_collar());
			i++;
		}
	}
	
	public void xFrecuenciaCardiaca(PrintWriter pr, Date inicial, Date finale, boolean caracter)
	{
		Dog elegido = null;
		SimpleDateFormat form = new SimpleDateFormat("yyyy/MMMMM/dd hh:mm aaa");
		if(caracter = MENOR)
		{
			int menor = 452000000;
			for(Dog p : tablaPerros.getAll())
			{
				ListaDoblementeEncadenada<Event> esta = p.getEventosEntreFecha(inicial,finale,CARDIO,false);
				 
				for(Event e : esta)
				{
					int valor = e.getRitmo_cardiaco();
					if(valor<menor)
					{
						menor = valor;
						elegido = p;
					}
				}
				
			}
			pr.println("El perro con menor ritmo cardiaco entre "+  form.format(inicial) + " y "+ form.format(finale) +" fue: "+elegido.getNombre()+"("+ elegido.getCodigo_collar() +")con un ritmo cardiaco de: " + menor + " .");
		}
		else if(caracter = MAYOR)
		{
			int mayor = 0;
			for(Dog p : tablaPerros.getAll())
			{
				ListaDoblementeEncadenada<Event> esta = p.getEventosEntreFecha(inicial,finale,CARDIO,false);
				 
				for(Event e : esta)
				{
					int valor = e.getRitmo_cardiaco();
					if(valor>mayor)
					{
						mayor = valor;
						elegido = p;
					}
				}
				
			}
			pr.println("El perro con mayor ritmo cardiaco entre "+  form.format(inicial) + " y "+ form.format(finale) +" fue: "+elegido.getNombre()+"("+ elegido.getCodigo_collar() +")con un ritmo cardiaco de: " + mayor + " .");
		}
		
	}
	
	public void xActivo(PrintWriter pr, Date inicial, Date finale, boolean caracter)
	{
		Dog elegido = null;
		SimpleDateFormat form = new SimpleDateFormat("yyyy/MMMMM/dd hh:mm aaa");
		if(caracter = MENOR)
		{
			int menor = 452000000;
			for(Dog p : tablaPerros.getAll())
			{
				int valor = p.getHorasFueraCasa(inicial, finale);
				if(valor<menor)
				{
					menor = valor;
					elegido = p;
				}
				
				
			}
			pr.println("El perro mas sedentario entre "+  form.format(inicial) + " y "+ form.format(finale) +" fue: "+elegido.getNombre()+"("+ elegido.getCodigo_collar() +")con " + menor + " horas en casa.");
		}
		else if(caracter = MAYOR)
		{
			int mayor = 0;
			for(Dog p : tablaPerros.getAll())
			{
				int valor = p.getHorasFueraCasa(inicial, finale);
				if(valor>mayor)
				{
					mayor = valor;
					elegido = p;
				}				
			}
			pr.println("El perro mas activo entre "+  form.format(inicial) + " y "+ form.format(finale) +" fue: "+elegido.getNombre()+"("+ elegido.getCodigo_collar() +")con " + mayor + " horas fuera de casa.");
		}
		
	}

	public ColaDePrioridad<Double, Event> getColaPrioridad(boolean pTipo) {
		ColaDePrioridad<Double, Event> ans;
		if(pTipo==GEO) {
			ans = new ColaDePrioridad<>(true);
			for(Event e : listaEventos) {
				double prioridad = getDistancia(e.getUbicacion_actual_latitud(), e.getUbicacion_actual_longitud(), LATITUD_CENTRO, LONGITUD_CENTRO);
				ans.enqueue(e, prioridad);
			}
		}
		else {
			ans = new ColaDePrioridad<>(false);
			for(Event e : listaEventos) {
				double prioridad = tablaDuenios.get(e.getCodigo_collar()).getPrioridad();
				ans.enqueue(e, prioridad);
			}
		}
		
		return ans;
		
	}

	public TablaHash<Integer, Dog> getTablaPerros() {
		return tablaPerros;
	}

	public TablaHash<Integer, Zone> getListaZonas() {
		return tablaZonas;
	}	

	
	public double getDistancia(double lat1,double lon1, double lat2,double lon2) {
		double R = 6371000; 
		double ph1 = Math.toRadians(lat1);
		double ph2 = Math.toRadians(lat2);
		double dph = Math.toRadians(lat2-lat1);
		double dl = Math.toRadians(lon2-lon1);

		double a = Math.sin(dph/2) * Math.sin(dph/2) +
		        Math.cos(ph1) * Math.cos(ph2) *
		        Math.sin(dl/2) * Math.sin(dl/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		double d = R * c;
		
		return d;
	}

	public TablaHash<Integer, Owner> getTablaDuenios() 
	{
		return tablaDuenios;
	}

	public ColaDePrioridad<Integer, Dog> generarColaFrecuencia() 
	{
		ColaDePrioridad<Integer, Dog> resp = new ColaDePrioridad<>(true);
		for(Dog p :tablaPerros.getAll())
		{
			resp.enqueue(p, p.getCantEmergencias());
		}
		return resp;
	}

	public ColaDePrioridad<Double, Event> generarColaNormalizada() 
	{
		ColaDePrioridad<Double, Event> resp = new ColaDePrioridad<>(true);
		for(Event e :listaEventos)	
		{
			double llave = getPrioNCardio(e) +getPrioNDistancia(e)+ getPrioNDuenio(e);
			resp.enqueue(e, llave);
		}
		
		return resp;
	}
	public double getPrioNDuenio(Event e)
	{
		
		return (double)(10-(tablaDuenios.get((Integer)e.getCodigo_collar()).getPrioridad()))/(double)10;
	}
	public double getPrioNDistancia(Event e)
	{
		Zone z = tablaZonas.get(tablaPerros.get(e.getCodigo_collar()).getZona());
		double dist = getDistancia(z.getUbicacion_max_noroccidente_latitud(), z.getUbicacion_max_noroccidente_longitud(),LATITUD_CENTRO , LONGITUD_CENTRO);	
		double otro = getDistancia(z.getUbicacion_max_suroriente_latitud(), z.getUbicacion_max_suroriente_longitud(),LATITUD_CENTRO , LONGITUD_CENTRO);
		if(otro>dist)
			dist = otro;
		otro = getDistancia(z.getUbicacion_max_suroriente_latitud(), z.getUbicacion_max_noroccidente_longitud(),LATITUD_CENTRO , LONGITUD_CENTRO);
		if(otro>dist)
			dist = otro;
		otro = getDistancia(z.getUbicacion_max_noroccidente_latitud(), z.getUbicacion_max_suroriente_longitud(),LATITUD_CENTRO , LONGITUD_CENTRO);
		if(otro>dist)
			dist = otro;
		return getDistancia(e.getUbicacion_actual_latitud(),e.getUbicacion_actual_longitud(),Admin.LATITUD_CENTRO,Admin.LONGITUD_CENTRO)/dist;
	}
	public double getPrioNCardio(Event e)
	{		
		return (double)e.getRitmo_cardiaco()/(double)(tablaPerros.get(e.getCodigo_collar()).getPul_max_pormin());
	}

    public Arbol23<String, ListaDoblementeEncadenada<Event>> getHistorial() {
		return historial;
	}	
}
