package mundo;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Alejandro on 10/02/16.
 */
public class Event implements IIdentificable{

    private int codigo_collar;
    private long fecha_hora;
    private int ritmo_Cardiaco;
    private double ubicacion_actual_latitud;
    private double ubicacion_actual_longitud;
    private String paso_puerta;
    
    private boolean alarmaGeo;
    private boolean alarmaCardio;

    public Event(int codigo_collar, long fecha_hora, int ritmo_Cardiaco, double ubicacion_actual_latitud, double ubicacion_actual_longitud, String paso_puerta) {
        this.codigo_collar = codigo_collar;

        this.fecha_hora = fecha_hora;

        this.ritmo_Cardiaco = ritmo_Cardiaco;
        this.ubicacion_actual_latitud = ubicacion_actual_latitud;
        this.ubicacion_actual_longitud = ubicacion_actual_longitud;
        this.paso_puerta = paso_puerta;
    }

    public int getCodigo_collar() {
        return codigo_collar;
    }

    public Date getFecha_hora() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(fecha_hora);
        return cal.getTime();
    }

    public int getRitmo_cardiaco() {
        return ritmo_Cardiaco;
    }

    public double getUbicacion_actual_latitud() {
        return ubicacion_actual_latitud;
    }

    public double getUbicacion_actual_longitud() {
        return ubicacion_actual_longitud;
    }

    public String getPaso_puerta() {
        return paso_puerta;
    }

    public boolean isAlarmaGeo() {
        return alarmaGeo;
    }
    public void setAlarmaGeo(boolean p)
    {
    	alarmaGeo = p;
    }
    public boolean isAlarmaCardio() {
        return alarmaCardio;
    }
    public void setAlarmaCardio(boolean p)
    {
    	alarmaCardio = p;
    }
	@Override
	public String getId() {
		
		return  fecha_hora+"";
	}

	@Override
	public int compareTo(Object o) {
		Event e = (Event)o;
		return (new Long(getId())).compareTo(Long.parseLong(e.getId()));
	}
}
