package testEstructuras;

import Estructuras.ColaDePrioridad;
import junit.framework.TestCase;
import mundo.Zone;

public class TestColaDPrioridad extends TestCase {
	
	private ColaDePrioridad<Integer, Zone> colaAlMayor;
	private ColaDePrioridad<Integer, Zone> colaAlMenor;
	
	public void setUp() {
		 colaAlMayor = new ColaDePrioridad<>(true);
		 colaAlMenor = new ColaDePrioridad<>(false);
	}
	
	public void testEncolar() {
		assertEquals("La cola no debería tener nada en ella", colaAlMayor.size(), 0);
		assertEquals("La cola no debería tener nada en ella (Al menor)", colaAlMenor.size(), 0);
		
		Zone z1 = new Zone(1, "Zona1", "Zona1", 1234, 12345, 1234, 1234);
		Zone z2 = new Zone(2, "Zona2", "Zona2", 1234, 12345, 1234, 1234);
		
		colaAlMayor.enqueue(z1, z1.getZona());
		colaAlMayor.enqueue(z2, z2.getZona());
		
		assertEquals("La cola no debería tener dos elementos", colaAlMayor.size(), 2);
		
		colaAlMenor.enqueue(z1, z1.getZona());
		colaAlMenor.enqueue(z2, z2.getZona());
		
		assertEquals("La cola no debería tener dos elementos", colaAlMenor.size(), 2);
	}
	
	public void testDesEncolar() {
		
		while(!colaAlMayor.isEmpty()) { colaAlMayor.unqueueMax();}
		while(!colaAlMenor.isEmpty()) { colaAlMenor.unqueueMax();}
		
		Zone z1 = new Zone(1, "Zona1", "Zona1", 1234, 12345, 1234, 1234);
		Zone z2 = new Zone(2, "Zona2", "Zona2", 1234, 12345, 1234, 1234);
		
		colaAlMayor.enqueue(z1, z1.getZona());
		colaAlMayor.enqueue(z2, z2.getZona());
		
		colaAlMenor.enqueue(z1, z1.getZona());
		colaAlMenor.enqueue(z2, z2.getZona());
		
		assertEquals("El elemento con mayor prioridad debería ser la zona 1(Al Menor)", colaAlMenor.unqueueMax().getPaseador_nombre(),z1.getPaseador_nombre() );
		assertEquals("El elemento con mayor prioridad debería ser la zona 2", colaAlMayor.unqueueMax().getPaseador_nombre(),z2.getPaseador_nombre() );
	}

}
