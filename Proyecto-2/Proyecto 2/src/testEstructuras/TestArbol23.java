package testEstructuras;

import Estructuras.*;
import junit.framework.TestCase;
import mundo.Owner;
import mundo.*;

public class TestArbol23 extends TestCase {
	
	private Arbol23<Integer, String> principal;
	
	public void setEscenario1() 
	{
		principal = new Arbol23<>();
	}
	public void setEscenario2() 
	{
		principal = new Arbol23<>();
		principal.agregar("q", 1);
		principal.agregar("w", 2);
		principal.agregar("e", 3);
		principal.agregar("r", 4);
		principal.agregar("t", 5);
		principal.agregar("y", 6);
		principal.agregar("u", 7);
		principal.agregar("i", 8);
		principal.agregar("o", 9);
		principal.agregar("p", 10);
		principal.agregar("a", 11);
		principal.agregar("s", 12);
		principal.agregar("d", 13);
		principal.agregar("f", 14);
		principal.agregar("g", 15);
		principal.agregar("h", 16);
	}
	
	public void testInicializador()
	{
		setEscenario1();
		assertNotNull("No deberia ser null", principal);
	}
	
	public void testAgregar()
	{
		setEscenario2();
		assertNotSame("El arbol no tiene elementos", null, principal.getRaiz());
		NodoArbol23<Integer, String> anali = principal.getRaiz();
		int altura = 1;
		while(anali.getDerecho()!= null)
		{
			anali = anali.getDerecho();
			if(anali.getColor()==NodoArbol23.NEGRO)
				altura++;				
		}
		
	}
	public void testBuscar()
	{
		setEscenario2();
		assertEquals("El elemento no es el correcto al buscar","q" , principal.buscar(1).getItem());
	}
	public void testSize()
	{
		setEscenario2();
		assertEquals("El tama�o deberia ser el correcto", 16, principal.size());
	}
}
