package testEstructuras;

import mundo.Zone;
import Estructuras.ListaDoblementeEncadenada;
import junit.framework.TestCase;

public class ListaDEncadenadaTest extends TestCase{
	
	private ListaDoblementeEncadenada<Zone> lista;
	
	public void setUp() {
		lista = new ListaDoblementeEncadenada<Zone>();
	}
	
	private void scenary1(){
		lista.add(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
	}
	
	private void scenary2() {
		lista.clear();
		lista.add(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
		lista.add(new Zone(1, "Carlos", "Torres", 34.00128390, -12.03930903933, 11,12098));
		lista.add(new Zone(2, "Jimena", "Torres", 34.00128390, -12.03930903933, 11,12098));
	}
	
	public void testIsEmpty() {
		assertEquals("La lista debería estar vacía", true, lista.isEmpty());
	}
	
	public void testAdd() {
		scenary1();
		assertEquals("La lista debería contener un objeto", false, lista.isEmpty());
	}
	
	
	
	public void testClear() {
		scenary1();
		lista.clear();
		assertEquals("La lista debería estar vacía", true, lista.isEmpty());
	}
	
	public void testSize() {
		scenary2();
		assertEquals("La lista debería tener 3 objetos", 3, lista.size());
	}
	
	public void testRemove() {
		lista.clear();
		scenary1();
		assertEquals("La lista debería estar vacía", true,lista.remove(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098)) );
	}
	
	public void testGetHead() {
		lista.clear();
		scenary2();
		assertEquals("Retorna el elemento equeivocado", 2, lista.getHead().getZona());
	}
	
	public void testGetTail() {
		scenary2();
		assertEquals("Retorna el elemento equeivocado", 0, lista.getTail().getZona());
	}

}
