package testEstructuras;

import Estructuras.*;
import junit.framework.TestCase;
import mundo.Owner;
import mundo.*;

public class TestHashTable extends TestCase {
	
	private TablaHash<Integer, Owner> tablaNormal;
	private TablaHash<Integer, Owner> tablaParam;
	
	public void setUp() {
		tablaNormal=new TablaHash<Integer, Owner>();
		tablaParam=new TablaHash<Integer, Owner>(100,(float)0.5);		
	}
	
	public void testPutTabla() {
		
		for(int i = 0; i < 100;i++) {
			int[] dd1 = {i};
			Owner c = new Owner("apellidos"+i, "nombres"+i, i, i, i, dd1, "email"+i, i, i%10+1);
			tablaNormal.put(c.getCodigo_collar()[0], c);
			tablaParam.put(c.getCodigo_collar()[0], c);
		}
		
		assertEquals("No se agregaron la cantidad correcta de contactos en la tabla sin prámetros",100, tablaNormal.size());
		assertEquals("No se agregaron la cantidad correcta de contactos en la tabla CON prámetros",100, tablaParam.size());
	}
	public void testDeleteTabla() {
		for(int i = 0; i < 100;i++) {
			tablaNormal.delete(i);
			tablaParam.delete(i);
		}
		assertEquals("No deberían quedar contactos en la tabla sin parámetros",0, tablaNormal.size());
		assertEquals("No deberían quedar contactos en la tabla CON parámetros",0, tablaParam.size());
	}
	public void testGetFromTable() {
		int[] dd1 = {1};
		int[] dd2 = {2};
		Owner d1 = new Owner("apellidos"+1, "nombres"+1, 1, 1, 1, dd1, "email"+1, 1, 1%10+1);
		Owner d2 = new Owner("apellidos"+2, "nombres"+2, 2, 2, 2, dd2, "email"+2, 2, 2%10+1);
				
		tablaNormal.put(d1.getCodigo_collar()[0],d1);
		assertEquals("Debería regresar el nombre del duenio 1",tablaNormal.get(d1.getCodigo_collar()[0]).getNombres(), d1.getNombres());
		tablaParam.put(d2.getCodigo_collar()[0],d2);
		assertEquals("Debería regresar el nombre del ciudadano 1 (TABLA PARAM)",tablaParam.get(d2.getCodigo_collar()[0]).getNombres(), d2.getNombres());
	}
}
