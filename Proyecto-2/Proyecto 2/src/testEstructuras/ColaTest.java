package testEstructuras;

import mundo.Zone;
import Estructuras.Cola;
import junit.framework.TestCase;

public class ColaTest extends TestCase{

	private Cola<Zone> cola;
	
	public void setUp() {
		 cola = new Cola<>();
		 
	}
	
	private void scenary1(){
		cola.add(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
	}
	
	private void scenary2() {
		cola.clear();
		cola.add(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
		cola.add(new Zone(1, "Carlos", "Torres", 34.00128390, -12.03930903933, 11,12098));
		cola.add(new Zone(2, "Jimena", "Torres", 34.00128390, -12.03930903933, 11,12098));
	}
	
	public void testIsEmpty() {
		assertEquals("La lista debería estar vacía", true, cola.isEmpty());
	}
	
	public void testAddEncolar() {
		scenary1();
		assertEquals("La lista debería contener un objeto", false, cola.isEmpty());
	}
	
	public void testClear() {
		scenary1();
		cola.clear();
		assertEquals("La lista debería estar vacía", true, cola.isEmpty());
	}
	
	public void testSize() {
		scenary2();
		assertEquals("La lista debería tener 3 objetos", 3, cola.size());
	}
	
	public void testRemoveDesencolar() {
		cola.clear();
		scenary1();
		cola.remove(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
		assertEquals("La lista debería estar vacía", true, cola.isEmpty());
		
		scenary2();
		cola.desencolar();
		assertEquals("La lista debería tener 2 objetos", 2, cola.size());
	}
	
	public void testGetFirst() {
		cola.clear();
		scenary2();
		assertEquals("Retorna el elemento equeivocado", 0, cola.getFirst().getZona());
	}
	
	public void testGetLast() {
		scenary2();
		assertEquals("Retorna el elemento equeivocado", 2, cola.getLast().getZona());
	}
}
