package testEstructuras;


import mundo.Zone;
import Estructuras.Pila;
import junit.framework.TestCase;

public class PilaTest extends TestCase
{
private Pila<Zone> lista;
	
	public void setUp() {
		lista = new Pila<Zone>();
	}
	
	private void scenary1(){
		lista.add(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
	}
	
	private void scenary2() {
		lista.clear();
		lista.add(new Zone(0, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
		lista.add(new Zone(1, "Carlos", "Torres", 34.00128390, -12.03930903933, 11,12098));
		lista.add(new Zone(2, "Jimena", "Torres", 34.00128390, -12.03930903933, 11,12098));
	}
	private void scenary3(){
		lista.push(new Zone(46, "David", "Torres", 34.00128390, -12.03930903933, 11,12098));
	}
	
	public void testIsEmpty() {
		assertEquals("La lista debería estar vacía", true, lista.isEmpty());
	}
	
	public void testAdd() {
		scenary1();
		assertEquals("La lista debería contener un objeto", false, lista.isEmpty());
	}
	
	
	
	public void testClear() {
		scenary1();
		lista.clear();
		assertEquals("La lista debería estar vacía", true, lista.isEmpty());
	}
	
	public void testSize() {
		scenary2();
		assertEquals("La lista debería tener 3 objetos", 3, lista.size());
	}
	
	public void testPop() {
		lista.clear();
		scenary2();
		Zone t =lista.pop();
		assertEquals("La lista debería tener 2 objetos", 2,lista.size() );
		assertEquals("t deberia ser la tercera zona", 2,t.getZona() );
	}
	public void testPush() {
		lista.clear();
		scenary3();
		assertEquals("La lista debería tener 1 objeto", 1,lista.size() );
		Zone t =lista.pop();
		assertEquals("el objeto deberia ser la zona agregada", 46,t.getZona() );
	}
	
	

}
