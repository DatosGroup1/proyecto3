package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import mundo.Admin;

public class MainA {

	public static void main(String[] args) throws Exception{

		System.out.println("Cliente PY1 v1.0");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 PrintWriter pr  = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);

		long time = System.currentTimeMillis();

		Admin mundo = new Admin(pr);

		
		

		boolean seguir = true;
		while (seguir){
			System.out.println("Seleccione la parte del proyecto que desea probar:");
			System.out.println("A: Análisis Cardiaco");
			System.out.println("B: Análisis Geográfico");
			System.out.println("C: Análisis Unificado");
			System.out.println("F: Finalizar");
			System.out.println("Seleccionar:");

			String opcion = br.readLine();

			switch (opcion) {
			case "A":
				ParteAA A = new ParteAA();
				A.realizarPruebas(mundo, pr);
				break;
			case "B":
				ParteBA B = new ParteBA();
				B.realizarPruebas(mundo, pr);
				break;
			case "C":
				ParteCA C = new ParteCA();
				C.realizarPruebas(mundo, pr);
				break;
			case "F":
				seguir = false;
				System.out.println("Cerrando cliente");
				break;
			default:
				break;
			}

		}
	}
	
	private static long imprimirTiempo(long time) {
		double segundos = (System.currentTimeMillis()-time)/1000;
		System.out.println("Tiempo: "+segundos+" segundos.");
		return System.currentTimeMillis();
	}

}
