package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mundo.Admin;

public class ParteCA {
	public void realizarPruebas(Admin mundo, PrintWriter pr) throws IOException
	{
		boolean enEjecucion=true;
		BufferedReader br; 
		while(enEjecucion)
		{
			imprimirMenu();
			br= new BufferedReader(new InputStreamReader(System.in));
			String caso = br.readLine();
			Date fechaInicial = null;
			Date  fechaFinal = null;
			String idPerro;
			double horasSinSubir;
			String actividad;
			long time ;
			
			switch(caso)
			{
			case "RF1":
				System.out.println("RF1: Consultar perros que no fueron recogidos en orden inverso");
				time= System.currentTimeMillis();
				mundo.perrosNoEntregados(pr);
				time = imprimirTiempo(time);
				break;
			case "RF2":
				System.out.println("RF2:Consultar perros cuya frecuencia cardiaca no ha aumentado");
				System.out.println("N�mero de horas de ejercicio");
				horasSinSubir=Double.parseDouble(br.readLine());
				time= System.currentTimeMillis();
				mundo.sinEjercicioHoras(pr, (int)horasSinSubir);
				time = imprimirTiempo(time);
				break;
			case "RF3":
				try {
					System.out.println("Fecha inicial yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					 fechaInicial=obtenerFecha(br);
					System.out.println("Fecha final yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
				fechaFinal=obtenerFecha(br);
						
				} catch (Exception e) {}
				System.out.println("RF3:Consultar el perro con menor frecuencia cardiaca sobre un periodo de tiempo");
				time= System.currentTimeMillis();
				mundo.xFrecuenciaCardiaca(pr, fechaInicial, fechaFinal, Admin.MENOR);
				time = imprimirTiempo(time);
				break;
			case "RF4":
				try {
					System.out.println("Fecha inicial yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					 fechaInicial=obtenerFecha(br);
					System.out.println("Fecha final yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
				fechaFinal=obtenerFecha(br);
						
				} catch (Exception e) {}
				System.out.println("RF4:Consultar el perro con mayor frecuencia cardiaca sobre un periodo de tiempo.");
				time= System.currentTimeMillis();

				mundo.xFrecuenciaCardiaca(pr, fechaInicial, fechaFinal, Admin.MAYOR);
				break;
			case "RF5":
				
				
				System.out.println("RF5:Consultar el perro m�s sedentario o que permaneci� m�s tiempo en casa");
				time= System.currentTimeMillis();

				mundo.xActivo(pr, fechaInicial, fechaFinal, Admin.MENOR);
				time = imprimirTiempo(time);
				break;
			case "RF6":
				System.out.println("RF6:Consultar el perro m�s activo o que permaneci� m�s tiempo fuera de casa");
				time= System.currentTimeMillis();
				mundo.xActivo(pr, fechaInicial, fechaFinal, Admin.MAYOR);
				time = imprimirTiempo(time);
				break;
			case "RF7":
				
				try {
					
					
					System.out.println("Id del perro");
					br = new BufferedReader(new InputStreamReader(System.in));
					idPerro=br.readLine();
						
				} catch (Exception e) {}
				System.out.println("RF7:Consultar actividades diarias realizadas por un perro en particular");
				time= System.currentTimeMillis();
				
				time = imprimirTiempo(time);
				break;
			case "RF8":
				try {
					System.out.println("Fecha inicial yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					 fechaInicial=obtenerFecha(br);
					System.out.println("Fecha final yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					 fechaFinal=obtenerFecha(br);
					 
					 System.out.println("Actividad a consultar");
						br = new BufferedReader(new InputStreamReader(System.in));
						actividad=br.readLine();
					
					System.out.println("Id del perro");
					br = new BufferedReader(new InputStreamReader(System.in));
					idPerro=br.readLine();
						
				} catch (Exception e) {}
				System.out.println("RF8: Consultar promedio de frecuencia cardiaca realizando una actividad durante un periodo de tiempo para un perro");
				time= System.currentTimeMillis();
				//TODO Implementar el m�todo de su mundo 
				//mundo.promedioDeActividad(fechaInicial,fechaFinal,actividad,IdPerro) 
				//Una actividad puede ser: Sali�, Regres�, De paseo, Haciendo ejercicio, durmiendo etc. 
				//Imprimir en consola Actividad:Promedio de frecuencia cardiaca
				time = imprimirTiempo(time);
				break;
			case "regresar":
				realizarPruebas(mundo, pr);
				break;
			case "exit":
				enEjecucion=false;
				break;	
		}
	}

	}
	
	private long imprimirTiempo(long time) {
		double segundos = (System.currentTimeMillis()-time)/1000;
		System.out.println("Tiempo: "+segundos+" segundos.");
		return System.currentTimeMillis();
	}
	
	private void imprimirMenu(){
		System.out.println("Cliente Parte C v1.0");
		System.out.println("Seleccione un requerimiento para probar:");
		System.out.println("RF1: Consultar perros que no fueron recogidos en orden inverso");
		System.out.println("RF2:Consultar perros cuya frecuencia cardiaca no ha aumentado");
		System.out.println("RF3:Consultar el perro con menor frecuencia cardiaca sobre un periodo de tiempo.");
		System.out.println("RF4: Consultar el perro con mayor frecuencia cardiaca sobre un periodo de tiempo. ");
		System.out.println("RF5: Consultar el perro m�s sedentario o que permaneci� m�s tiempo en casa. ");
		System.out.println("RF6: Consultar el perro m�s activo o que permaneci� m�s tiempo fuera de casa. ");
		System.out.println("RF7: Consultar actividades diarias realizadas por un perro en particular. ");
		System.out.println("RF8: Consultar promedio de frecuencia cardiaca realizando una actividad durante un periodo de tiempo para un perro. ");
		System.out.println("Regresar: Probar otro requerimiento");
		System.out.println("Exit: Salir");
		
	}
	private Date obtenerFecha(BufferedReader br) throws Exception{
		System.out.println("Ingrese una fecha siguiendo el formato: yyyy-MM-dd (2015-12-24)");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.parse(br.readLine());
	}
}
