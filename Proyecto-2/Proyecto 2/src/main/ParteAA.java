package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mundo.Admin;
import mundo.Dog;

public class ParteAA {
	
	public void realizarPruebas(Admin mundo, PrintWriter pr){
		long time = System.currentTimeMillis();

		try {
			boolean seguir = true;
			while (seguir){
				imprimirMenu();
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String opcion = br.readLine();
				time = System.currentTimeMillis();
				Date fechaInicio;
				Date fechaFin;
				String id;
				
				Dog perro = null;
				switch (opcion) {
				case "RF1":
					fechaInicio = obenerFecha(br);
					fechaFin = obenerFecha(br);
					if (fechaInicio.compareTo(fechaFin)>0){
						throw new Exception("La fecha de inicio no puede ser mayor a la de final");
					}
					time = System.currentTimeMillis();
					perro =mundo.perroMasAlarmado(Admin.CARDIO, fechaInicio, fechaFin);
					System.out.println("Perro con mas alarmas en el rango: "+perro);
					time = imprimirTiempo(time);
					break;
				case "RF2":
					System.out.println("Ingrese el ID del perro que desea buscar: ");
					id = br.readLine();
					fechaInicio = obenerFecha(br);
					fechaFin = obenerFecha(br);
					time = System.currentTimeMillis();
					mundo.historialDiario(pr, fechaInicio,fechaFin, Integer.parseInt(id));

					time = imprimirTiempo(time);
					break;
				case "RF3":
					System.out.println("Ingrese el ID del perro que desea buscar: ");
					id = br.readLine();
					fechaInicio = obenerFecha(br);
					fechaFin = obenerFecha(br);
					time = System.currentTimeMillis();
					double promedio = 0;
					promedio = mundo.promedioDatos(Admin.CARDIO, pr, Integer.parseInt(id));
					System.out.println("El promedio para el perro con ID: "+id+" es "+promedio);
					time = imprimirTiempo(time);
					break;
				case "RF4":
					System.out.println("Ingrese el ID del perro que desea buscar: ");
					id = br.readLine();
					time = System.currentTimeMillis();
					int frecuencia = 0;
					frecuencia = mundo.valorReciente(pr, Integer.parseInt(id)).getRitmo_cardiaco();
					System.out.println("La ultima lectura cardiaca para el perro con ID: "+id+" es "+frecuencia);
					time = imprimirTiempo(time);
					break;
				case "Exit":
					seguir = false;
					break;

				default:
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage()+" en la parte A");
		}
	}

	private long imprimirTiempo(long time) {
		double segundos = (System.currentTimeMillis()-time)/1000;
		System.out.println("Tiempo: "+segundos+" segundos.");
		return System.currentTimeMillis();
	}

	private void imprimirMenu(){
		System.out.println("Cliente Parte A v1.0");
		System.out.println("Seleccione un requerimiento para probar:");
		System.out.println("RF1: Perro con mas alarmas en un rango de fechas");
		System.out.println("RF2: Historial de frecuencia cardiaca de un perro en un rango de fechas");
		System.out.println("RF3: Promedio de frecuencia cardiaca de un perro en un rango de fechas");
		System.out.println("RF4: Valor mas reciente de frecuencia cardiaca de un perro");
		System.out.println("Exit: Terminar las pruebas (Menu principal)");
		System.out.println("Ingrese una opcion: ");
	}

	private Date obenerFecha(BufferedReader br) throws Exception{
		System.out.println("Ingrese una fecha siguiendo el formato: yyyy-MM-dd (2015-12-24)");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.parse(br.readLine());
	}
}
