package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import Estructuras.ColaDePrioridad;
import mundo.Admin;
import mundo.Event;
import mundo.Dog;

public class ParteC {

	public void realizarPruebas(Admin mundo) throws IOException
	{
		boolean enEjecucion=true;
		BufferedReader br; 
		while(enEjecucion)
		{
			imprimirMenu();
			br= new BufferedReader(new InputStreamReader(System.in));
			String caso = br.readLine();
			long time ;
			switch(caso)
			{
			
			case "RF1":
				time= System.currentTimeMillis();
				ColaDePrioridad<Integer, Dog> prio = mundo.generarColaFrecuencia();
				File f = new File("./docs");
				File t = new File(f,"datos.json");
				t.createNewFile();
				PrintWriter pw = new PrintWriter(new FileWriter(t));
				pw.print("[");
				Dog p=null;
				boolean vigilante1 = false;
				while(!prio.isEmpty())
				{
					if(vigilante1) {
						pw.print(",");
					}
					p = prio.unqueueMax();
					pw.print("{");
					pw.print("\"prioridad\":"+p.getCantEmergencias()+", \"dueno\":{\"nombre\":\""+mundo.getTablaDuenios().get(p.getCodigo_collar()).getNombres()+"\",\"apellido\":\""+mundo.getTablaDuenios().get(p.getCodigo_collar()).getApellidos()+"\"}, \"direccion\":{\"latitud\":"+mundo.getTablaDuenios().get(p.getCodigo_collar()).getDireccion_latitud()+",\"longitud\":"+mundo.getTablaDuenios().get(p.getCodigo_collar()).getDireccion_longitud()+"}, \"perro\":\""+p.getNombre()+"\"");
					pw.print("}");
					vigilante1 = true;
				}
				pw.println("]");
				pw.close();
				
				System.out.println("El reporte se ha generado satisfactoriamente");
				time = imprimirTiempo(time);
				break;

			case "RF2":
				time= System.currentTimeMillis();
				System.out.println("Ingrese el numero de eventos que desea visualizar: ");
				int num = Integer.parseInt(br.readLine());
				ColaDePrioridad<Double, Event> norm = mundo.generarColaNormalizada();
				File f1 = new File("./docs");
				File t1 = new File(f1,"datosNormalizado.json");
				t1.createNewFile();
				PrintWriter pw1 = new PrintWriter(new FileWriter(t1));
				pw1.print("[");
				Event e=null;
				boolean vigilante = false;
				for(int i = 0;i<num-1 && !norm.isEmpty();i++)
				{
					if(vigilante) {
						pw1.print(",");
					}
					e = norm.unqueueMax();
					pw1.print("{");
					pw1.print("\"prioridad\":"+(double)(mundo.getPrioNCardio(e) +mundo.getPrioNDistancia(e)+ mundo.getPrioNDuenio(e))+", \"prioridades especificas\":{\"prioridad duenio\":"+mundo.getPrioNDuenio(e)+",\"prioridad cardiaca\":"+mundo.getPrioNCardio(e)+",\"prioridad distancia\":"+mundo.getPrioNDistancia(e)+"}, \"perro\":\""+mundo.getTablaPerros().get(e.getCodigo_collar()).getNombre()+"\"");
					pw1.print("}");
					vigilante = true;
				}
				pw1.println("]");
				pw1.close();
				
				System.out.println("El reporte se ha generado satisfactoriamente");
				time = imprimirTiempo(time);
				break;
				
			case "exit":
				enEjecucion=false;
				break;	
		}
	}

	}
	
	private long imprimirTiempo(long time) {
		double segundos = (System.currentTimeMillis()-time)/1000;
		System.out.println("Tiempo: "+segundos+" segundos.");
		return System.currentTimeMillis();
	}
	
	private void imprimirMenu(){
		System.out.println("Cliente Parte C v1.0");
		System.out.println("Seleccione un requerimiento para probar:");
		System.out.println("RF1: Obtener JSON perros seg�n prioridad");
		System.out.println("RF2: Obtener JSON perros seg�n prioridad normalizada");
		System.out.println("Exit: Salir");
		
	}
}
