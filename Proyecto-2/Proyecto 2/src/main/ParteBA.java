package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mundo.Admin;
import mundo.Dog;

public class ParteBA {
	public void realizarPruebas(Admin mundo, PrintWriter pr) throws IOException
	{
		boolean enEjecucion=true;
		BufferedReader br; 

		while(enEjecucion)
		{
			imprimirMenu();
			br = new BufferedReader(new InputStreamReader(System.in));
			String caso = br.readLine();
			Date fechaInicial = null;
			Date  fechaFinal = null;
			String idPerro = null;
			long time ;
			switch(caso)
			{
			case "RF1":


				try {
					System.out.println("Fecha inicial yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					fechaInicial=obtenerFecha(br);
					System.out.println("Fecha final yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					fechaFinal=obtenerFecha(br);
				} catch (Exception e) {

				}
				System.out.println("RF1:Consultar el perro que m�s alarmas gener� en un rango de fechas");
				time= System.currentTimeMillis();
				Dog perro =mundo.perroMasAlarmado(Admin.CARDIO, fechaInicial, fechaFinal);
				System.out.println("Perro con mas alarmas en el rango: "+perro.getCodigo_collar());//perro.getDuenio().getCedula());
				time = imprimirTiempo(time);
				break;
			case "RF2":

				try {
					System.out.println("Fecha inicial yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					fechaInicial=obtenerFecha(br);
					System.out.println("Fecha final yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					fechaFinal=obtenerFecha(br);

					System.out.println("Id del perro");
					br = new BufferedReader(new InputStreamReader(System.in));
					idPerro=br.readLine();

				} catch (Exception e) {}
				System.out.println("RF2:Promedio de actividades realizadas a diario por un perro");
				time= System.currentTimeMillis();
				mundo.promedioDatos(Admin.GEO, pr, Integer.parseInt(idPerro));
				time = imprimirTiempo(time);
				break;
			case "RF3":

				try {
					System.out.println("Fecha inicial yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					fechaInicial=obtenerFecha(br);
					System.out.println("Fecha final yyyy-MM-dd");
					br = new BufferedReader(new InputStreamReader(System.in));
					fechaFinal=obtenerFecha(br);

					System.out.println("Id del perro");
					br = new BufferedReader(new InputStreamReader(System.in));
					idPerro=br.readLine();

				} catch (Exception e) {}
				System.out.println("RF3:Historial del recorrido diario realizado por un perro en un periodo de tiempo");
				time= System.currentTimeMillis();
				mundo.historialDiario(pr, fechaInicial,fechaFinal, Integer.parseInt(idPerro));

				time = imprimirTiempo(time);
				break;
			case "RF4":

				try {

					System.out.println("Id del perro");
					br = new BufferedReader(new InputStreamReader(System.in));
					idPerro=br.readLine();
				} catch (Exception e) {}
				System.out.println("RF4:Dar la �ltima  ubicaci�n de un perro");
				time= System.currentTimeMillis();
				double u = mundo.valorReciente(pr, Integer.parseInt(idPerro)).getUbicacion_actual_latitud();
				double ud = mundo.valorReciente(pr, Integer.parseInt(idPerro)).getUbicacion_actual_longitud();
				System.out.println("La ultima ubicacion para el perro con ID: "+idPerro+" es "+u+"/"+ud);
				time = imprimirTiempo(time);
				break;
			case "regresar":
				realizarPruebas(mundo, pr);
				break;
			case "exit":
				enEjecucion=false;
				break;
			}
		}

	}

	private void imprimirMenu(){
		System.out.println("Cliente Parte B v1.0");
		System.out.println("Seleccione un requerimiento para probar:");
		System.out.println("RF1: Consultar el perro que m�s alarmas gener� en un rango de fechas ");
		System.out.println("RF2:Promedio de actividades realizadas a diario por un perro");
		System.out.println("RF3:Historial del recorrido diario realizado por un perro en un periodo de tiempo");
		System.out.println("RF4: Dar la �ltima  ubicaci�n de un perro ");
		System.out.println("Regresar: Probar otro requerimiento");
		System.out.println("Exit: Salir");

	}
	private Date obtenerFecha(BufferedReader br) throws Exception{
		System.out.println("Ingrese una fecha siguiendo el formato: yyyy-MM-dd (2015-12-24)");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.parse(br.readLine());
	}
	private long imprimirTiempo(long time) {
		double segundos = (System.currentTimeMillis()-time)/1000;
		System.out.println("Tiempo: "+segundos+" segundos.");
		return System.currentTimeMillis();
	}
}
