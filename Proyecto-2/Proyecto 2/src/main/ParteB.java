package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import Estructuras.Cola;
import Estructuras.ColaDePrioridad;
import Estructuras.ListaDoblementeEncadenada;
import mundo.Admin;
import mundo.Owner;
import mundo.Event;
import mundo.Dog;

public class ParteB {
	
	
	public void realizarPruebas(Admin mundo){
		
		long time = System.currentTimeMillis();

		try {
			boolean seguir = true;
			while (seguir){
				imprimirMenu();
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				DecimalFormat df = new DecimalFormat("#.000000");
				SimpleDateFormat form = new SimpleDateFormat("yyyy/MMMMM/dd hh:mm aaa");
				SimpleDateFormat form1 = new SimpleDateFormat("yyyy/MMMMM/dd");
				String opcion = br.readLine();
				time = System.currentTimeMillis();
				String id;
				Date fechaEvento;
				StringBuilder sb = new StringBuilder("");
				switch (opcion) {
				
				case "RF1":

					time = System.currentTimeMillis();
					ColaDePrioridad<Double, Event> cola = mundo.getColaPrioridad(Admin.GEO);
			        sb.append("|                                                                                     \n");
			        sb.append("| EVENTOS POR PRIORIDAD GEOGRAFICA:                                                   \n");
			        sb.append("|                                                                                     \n");
		        	sb.append("---------------------------------------------------------------------------------------\n");
			        while(!cola.isEmpty()) {
			        	Event e = cola.unqueueMax();
			        	sb.append("|  #COLLAR ASOCIADO: "+e.getCodigo_collar()+ "                                        \n");
			        	sb.append("|  FECHA: "+form.format(e.getFecha_hora())+ "                                         \n");
			        	sb.append("|  LATITUD: "+df.format(e.getUbicacion_actual_latitud())+ "                           \n");
			        	sb.append("|  LONGITUD: "+df.format(e.getUbicacion_actual_longitud())+ "                         \n"); 
			        	sb.append("|  PULSO REGISTRADO: "+e.getRitmo_cardiaco()+ "                                       \n");
			        	sb.append("|  DISTANCIA DEL CENTRO DE ATENCION: "+ mundo.getDistancia(e.getUbicacion_actual_latitud(),e.getUbicacion_actual_longitud(),Admin.LATITUD_CENTRO,Admin.LONGITUD_CENTRO)+ "                                         \n");
			        	sb.append("---------------------------------------------------------------------------------------\n");						
			        }
			        System.out.println(sb.toString());
			        time = imprimirTiempo(time);
					break;
					
				case "RF2":
					
					System.out.println("Ingrese el ID del perro en peligro: ");
					id = br.readLine();
     				time = System.currentTimeMillis();
     				Dog p = mundo.getTablaPerros().get(Integer.parseInt(id));
     				boolean bien = true;
 					if(p.getCantEmergencias()!= 0)
 						bien = false;
     				if(bien)
     				{
     					System.out.println("El perro no esta en peligro");
     				}
     				else
     				{
     					Owner d = mundo.getTablaDuenios().get(Integer.parseInt(id));
     					sb.append("|                                                                                     \n");
    			        sb.append("| DUE�O DEL PERRO DE CODIGO "+id+":                                                    \n");
    			        sb.append("|                                                                                     \n");
    		        	sb.append("---------------------------------------------------------------------------------------\n");	
    		        	sb.append("|  NOMBRE: "+ d.getNombres()+ "                                                      \n");
			        	sb.append("|  APELLIDOS: "+d.getApellidos()+ "                                                   \n");
			        	sb.append("|  CEDULA: "+d.getCedula()+ "                                                         \n");
			        	sb.append("|  DIRECCION LATITUD: "+df.format(d.getDireccion_latitud())+ "                        \n");
			        	sb.append("|  DIRECCION LONGITUD: "+df.format(d.getDireccion_longitud())+ "                      \n"); 
			        	sb.append("|  CELULAR: "+d.getCelular()+ "                                                       \n");
			        	sb.append("|  EMAIL: "+ d.getEmail()+ "                                                          \n");
			        	sb.append("|  PRIORIDAD: "+ d.getPrioridad()+ "                                                  \n");
			        	sb.append("---------------------------------------------------------------------------------------\n");						
    			        
     				}
     				System.out.println(sb.toString());
					time = imprimirTiempo(time);
					break;
					
					
				case "RF3":
					System.out.println("Ingrese el ID del perro de interes: ");
					id = br.readLine();
					System.out.println("Ingrese la fecha de acontecimiento del evento: ");
					fechaEvento = obenerFecha(br);
					time = System.currentTimeMillis();
					String llave = (fechaEvento.getYear()+1900)+"-"+(fechaEvento.getMonth()+1)+"-"+fechaEvento.getDate()+"-"+id;
					ListaDoblementeEncadenada<Event> even = mundo.getHistorial().buscar(llave).getItem();
					Dog pe = mundo.getTablaPerros().get(Integer.parseInt(id));
					sb.append("|                                                                                     \n");
			        sb.append("| EVENTOS DEL PERRO DE CODIGO"+id+" EN LA FECHA "+form1.format(fechaEvento)+":        \n");
			        sb.append("|                                                                                     \n");
		        	sb.append("---------------------------------------------------------------------------------------\n");	
		        	sb.append("|  NOMBRE: "+pe.getNombre()+ "                                                        \n");
		        	sb.append("|  EDAD: "+pe.getEdad_meses()+ "                                                      \n");
		        	sb.append("|  RAZA: "+pe.getRaza()+ "                                                            \n");
		        	sb.append("|  ZONA: "+pe.getZona()+ "                                                            \n");
		        	for(Event e : even)
					{						
		        		sb.append("\n|  #COLLAR ASOCIADO: "+e.getCodigo_collar()+ "                                        \n");
			        	sb.append("|  FECHA: "+form.format(e.getFecha_hora())+ "                                         \n");
			        	sb.append("|  LATITUD: "+df.format(e.getUbicacion_actual_latitud())+ "                           \n");
			        	sb.append("|  LONGITUD: "+df.format(e.getUbicacion_actual_longitud())+ "                         \n"); 
			        	sb.append("|  PULSO REGISTRADO: "+e.getRitmo_cardiaco()+ "                                       \n");
			        	sb.append("---------------------------------------------------------------------------------------\n");						
			        }
		        	System.out.println(sb.toString());
		        	time = imprimirTiempo(time);
					break;
					
				case "Exit":
					seguir = false;
					break;

				default:
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage()+" en la parte A");
		}
	}

	private long imprimirTiempo(long time) {
		double segundos = (System.currentTimeMillis()-time)/1000;
		System.out.println("Tiempo: "+segundos+" segundos.");
		return System.currentTimeMillis();
	}

	private void imprimirMenu(){
		System.out.println("Cliente Parte B v1.0");
		System.out.println("Seleccione un requerimiento para probar:");
		System.out.println("RF1: Mostrar eventos de los perros en orden de prioridad");
		System.out.println("RF2: Buscar al due�o de un perro en peligro dado su id");
		System.out.println("RF3: Buscar los eventos asociados a un perro dada una fecha y el id del perro");
		System.out.println("Exit: Terminar las pruebas (Menu principal)");
		System.out.println("Ingrese una opcion: ");
	}

	private Date obenerFecha(BufferedReader br) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.parse(br.readLine());
	}
}
