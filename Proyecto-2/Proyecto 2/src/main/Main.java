package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import mundo.Admin;

public class Main {
	
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static long time = System.currentTimeMillis();
	
	
	public static void main(String[] args) throws Exception{
		PrintWriter pr  = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);
		System.out.println("Cliente PY2 v1.0");
		Admin mundo = new Admin(pr);

	    boolean seguir = true;
		while (seguir){
			System.out.println("Seleccione la parte del proyecto que desea probar:");
			System.out.println("A: Cargar archivo para realizar pruebas");
			System.out.println("B: Atenci�n preferencial a clientes");
			System.out.println("C: Atenci�n prioritaria por zonas");
			System.out.println("D: Generar reporte: seguimiento de la emergencia");
			System.out.println("E: Finalizar");
			System.out.println("Seleccionar:");

			String opcion = br.readLine();

			switch (opcion) {
			case "A":
			{
				System.out.println("Ingrese la ruta del archivo que desea cargar");
				String ruta = br.readLine();
				ruta=ruta.trim();
				menuCargarArchivos(ruta,mundo,pr);
				break;
			}
			case "B":
				ParteA A = new ParteA();
				A.realizarPruebas(mundo);
				break;
			case "C":
				ParteB B = new ParteB();
				B.realizarPruebas(mundo);
				break;
			case "D":
				ParteC C = new ParteC();
				C.realizarPruebas(mundo);
				break;
			case "E":
				seguir = false;
				System.out.println("Cerrando cliente");
				break;
			default:
				break;
			}

		}
	}
	public static void menuCargarArchivos(String ruta, Admin mundo, PrintWriter pr) throws Exception
	{
		System.out.println("Seleccione el tipo de archivo a cargar: ");
		System.out.println("1. Perros");
		System.out.println("2. Personas");
		System.out.println("3. Eventos");
		System.out.println("4. Zonas");
		System.out.println("5. Generales");
		System.out.println("6. Exit");
		
		String op = br.readLine();
		switch (op) {
		case "1":
		{   time = System.currentTimeMillis();
			mundo.inicializarPerro(pr, ruta);
			time = imprimirTiempo(time);   
		    break;
		}
		case "2":
			time = System.currentTimeMillis();
			mundo.inicializarDuenio(pr, ruta);
			time = imprimirTiempo(time);
			break;
		case "3":
			time = System.currentTimeMillis();
			mundo.inicializarEventos(pr, ruta);
			time = imprimirTiempo(time);
			break;
		case "4":
			time = System.currentTimeMillis();
			mundo.inicializarArea(pr, ruta);
			time = imprimirTiempo(time);
			break;
		case "5":
			time = System.currentTimeMillis();
			mundo.inicializarDuenio(pr, ruta+"/owners.json");
			mundo.inicializarPerro(pr, ruta+"/dogs.json");
			mundo.inicializarArea(pr, ruta+"/areas.json");
			time = imprimirTiempo(time);
			break;
		case "6":			
			break;
		}
	}
	
	private static long imprimirTiempo(long time) 
	{
		double segundos = (System.currentTimeMillis()-time)/1000;
		System.out.println("Tiempo: "+segundos+" segundos.");
		return System.currentTimeMillis();
	}

}
